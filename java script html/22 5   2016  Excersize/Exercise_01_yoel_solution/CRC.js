var carsArr;
function fillModels() {

	var toyotaModelsArr=["Avalon","Camry","Corolla","Yaris","Prius"];
	var suzukiModelsArr=["Baleno","Ignis","Vitara","Wagon","Jimny"];
	var kiaModelsArr=["Optima","Cadenza","K900","Soul","Sorento"];

	$('#Model').empty();
	var option = $('<option/>');
	option.attr({ 'value': "empty"}).text("..");
	$('#Model').append(option);
	if($("#Manufacturer>option:selected").text()=="Toyota"){

		for (var i = toyotaModelsArr.length - 1; i >= 0; i--) {

			var option = $('<option/>');
			option.attr({ 'value': toyotaModelsArr[i] }).text(toyotaModelsArr[i]);
			$('#Model').append(option);
		}
	}
	if($("#Manufacturer>option:selected").text()=="Suzuki"){

		for (var i = suzukiModelsArr.length - 1; i >= 0; i--) {

			var option = $('<option/>');
			option.attr({ 'value': suzukiModelsArr[i] }).text(suzukiModelsArr[i]);
			$('#Model').append(option);
		}
	}
	if($("#Manufacturer>option:selected").text()=="Kia"){

		for (var i = kiaModelsArr.length - 1; i >= 0; i--) {

			var option = $('<option/>');
			option.attr({ 'value': kiaModelsArr[i] }).text(kiaModelsArr[i]);
			$('#Model').append(option);
		}
	}
	
}

function showCars() {
	console.log("showCars");
	$('ul.list-group').empty();
	var tempCarArr=[];

	tempCarArr = carsArr.filter(filterByModelYearPrice);
			/*
			var Model= $("#Model>option:selected").attr("value");
			var Year=	$("#Year>option:selected").attr("value");
			var Price =$("#Price>option:selected").attr("value");
			for (var i = carsArr.length - 1; i >= 0; i--) {
				if(carsArr[i].Model == Model 
					&& carsArr[i].Year == Year 
					&& Number(carsArr[i].Price) < Number(Price)+10000
					&& Number(carsArr[i].Price) > Number(Price) ){
					tempCarArr.push(carsArr[i]);
				}
			}*/
			$.each(tempCarArr,function(i,car){
				$('ul.list-group').append('<li class="list-group-item">'+car.Manufacturer+' '+car.Model+' '+car.Year+' '+car.Price+'</li>');
			});

		}



		carsArr= [
		{
			"Manufacturer":"Toyota",
			"Model":"Avalon",
			"Year":"2014",
			"Price":"35000"
		},
		{
			"Manufacturer":"Toyota",
			"Model":"Camry",
			"Year":"2013",
			"Price":"35000"
		},
		{
			"Manufacturer":"Toyota",
			"Model":"Corolla",
			"Year":"2013",
			"Price":"35000"
		},
		{
			"Manufacturer":"Toyota",
			"Model":"Yaris",
			"Year":"2014",
			"Price":"35000"
		},
		{
			"Manufacturer":"Toyota",
			"Model":"Prius",
			"Year":"2014",
			"Price":"35000"
		},
		{
			"Manufacturer":"Suzuki",
			"Model":"Baleno",
			"Year":"2015",
			"Price":"40000"
		},
		{
			"Manufacturer":"Suzuki",
			"Model":"Ignis",
			"Year":"2011",
			"Price":"40000"
		},
		{
			"Manufacturer":"Suzuki",
			"Model":"Vitara",
			"Year":"2015",
			"Price":"40000"
		},
		{
			"Manufacturer":"Suzuki",
			"Model":"Wagon",
			"Year":"2015",
			"Price":"40000"
		},
		{
			"Manufacturer":"Suzuki",
			"Model":"Jimny",
			"Year":"2015",
			"Price":"40000"
		},
		{
			"Manufacturer":"Kia",
			"Model":"Optima",
			"Year":"2010",
			"Price":"20000"
		},
		{
			"Manufacturer":"Kia",
			"Model":"Cadenza",
			"Year":"2010",
			"Price":"20000"
		},
		{
			"Manufacturer":"Kia",
			"Model":"K900",
			"Year":"2010",
			"Price":"20000"
		},
		{
			"Manufacturer":"Kia",
			"Model":"Soul",
			"Year":"2010",
			"Price":"20000"
		},
		{
			"Manufacturer":"Kia",
			"Model":"Sorento",
			"Year":"2010",
			"Price":"20000"
		}
		];

		function filterByModelYearPrice(car) {
			var Model= $("#Model>option:selected").attr("value");
			var Year=	$("#Year>option:selected").attr("value");
			var Price =$("#Price>option:selected").attr("value");
			if(Model == "empty"&& Year == "empty"&& Price == "empty" ) return false;
			if(Model == "empty"&& Year == "empty"&& Price != "empty" )
				return ( Number(car.Price) < Number(Price)+10000
					&& Number(car.Price) >= Number(Price));
			if(Model == "empty"&& Year != "empty"&& Price == "empty" )
				return  car.Year == Year;
			if(Model != "empty"&& Year == "empty"&& Price == "empty" )
				return  car.Model == Model;

			if(Model != "empty"&& Year !== "empty"&& Price == "empty" )
				return  (car.Model == Model 
					&& car.Year == Year);
			if(Model != "empty"&& Year == "empty"&& Price !== "empty" )
				return  (car.Model == Model 			 
					&& Number(car.Price) < Number(Price)+10000
					&& Number(car.Price) >= Number(Price));
			if(Model == "empty"&& Year !== "empty"&& Price !== "empty" )
				return ( car.Year == Year 
					&& Number(car.Price) < Number(Price)+10000
					&& Number(car.Price) >= Number(Price));




			if(Model != "empty"&& Year != "empty"&& Price != "empty" )
				return  (car.Model == Model 
					&& car.Year == Year 
					&& Number(car.Price) < Number(Price)+10000
					&& Number(car.Price) >= Number(Price));



		}

		$( document ).ready( function() {

			console.log(carsArr);
		});