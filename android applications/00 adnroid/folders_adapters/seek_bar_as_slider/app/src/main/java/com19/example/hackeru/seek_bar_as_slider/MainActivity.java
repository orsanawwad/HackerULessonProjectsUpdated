package com19.example.hackeru.seek_bar_as_slider;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener , View.OnClickListener {

    SeekBar seek1;
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seek1 = (SeekBar)findViewById(R.id.seek1);
        b1 = (Button)findViewById(R.id.b1);
        seek1.setOnSeekBarChangeListener(this);


    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        b1.setText("value of seek bar "+progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

        Toast.makeText(getApplicationContext(),"touch began",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Toast.makeText(getApplicationContext(),"touch stoped",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {

    }
}
