package com19.example.hackeru.firebase33;

/**
 * Created by hackeru on 4/20/2016.
 */
public class product {
    private   String name;
    private  String code;
    private int price;

    @Override
    public String toString() {
        return "product{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", price=" + price +
                '}';
    }

    public product(String name, String code, int price) {
        this.name = name;
        this.code = code;
        this.price = price;
    }
    public product()
    {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
