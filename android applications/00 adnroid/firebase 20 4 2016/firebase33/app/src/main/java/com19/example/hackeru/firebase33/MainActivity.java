package com19.example.hackeru.firebase33;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText ed_name,ed_code,ed_price;
    Button btn_add,btn_show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Firebase.setAndroidContext(this);

        ed_name = (EditText)findViewById(R.id.ed_name);
        ed_code = (EditText)findViewById(R.id.ed_code);
        ed_price = (EditText)findViewById(R.id.ed_price);
        btn_add = (Button)findViewById(R.id.btn_add);
        btn_show = (Button)findViewById(R.id.btn_show);

    }
    public  void my_clik(View v1)
    {
        Firebase fb1 = new Firebase("https://ad11.firebaseio.com/");
        Firebase fbs2 = fb1.child("mahsan");
        Firebase products_= fbs2.child("products");
        product pr1 = new product(ed_name.getText().toString(),ed_code.getText().toString(),Integer.parseInt(ed_price.getText().toString()));
        products_.push().setValue(pr1);

    }
    public void click_show(View v1)
    {
        Firebase fb1 = new Firebase("https://ad11.firebaseio.com/");
        Firebase products_= fb1.child("products");

        products_.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<product> my_list = new ArrayList<product>();
                String all="";
                for (DataSnapshot ds1: dataSnapshot.getChildren())
                {
                    product pr1;
                    pr1 = ds1.getValue(product.class);
                    my_list.add(pr1);
                }
                btn_show.setText("");
                for (product pr:my_list)
                {
                    all += pr;
                }
                btn_show.setText(all);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }
}
