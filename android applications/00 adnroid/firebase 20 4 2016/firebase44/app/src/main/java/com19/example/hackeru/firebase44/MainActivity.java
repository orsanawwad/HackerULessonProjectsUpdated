package com19.example.hackeru.firebase44;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    car my_car= null;
    EditText ed_number,ed_comp,ed_price,ed_search;
    Button btn_add,btn_show;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);

        ed_price  = (EditText)findViewById(R.id.edPrice);
        ed_number  = (EditText)findViewById(R.id.ed_carNum);
        ed_comp  = (EditText)findViewById(R.id.ed_company);
        ed_search  = (EditText)findViewById(R.id.ed_search);
        btn_add = (Button)findViewById(R.id.btnAddCar);
        btn_show = (Button)findViewById(R.id.btn_search);
        getKeys();
    }

    public void addCar(car cr1)
    {
        Firebase fbs1 = new Firebase("https://ad11.firebaseio.com/");
         Firebase carsData = fbs1.child("cars");
        Firebase my_car = carsData.child(cr1.getCarNumber());
        my_car.setValue(cr1);

    }
    public  void  add_click(View v1)
    {
        car cr1;
        cr1 = new car(ed_number.getText().toString(),ed_comp.getText().toString(),Integer.parseInt(ed_price.getText().toString()));
        addCar(cr1);
    }
    public  void searchClick(View v1)
    {
        String carNum;
        carNum = ed_search.getText().toString();
       findCar(carNum.trim());
      //  btn_show.setText(cr1.toString());
    }
    public  void findCar(final String car_number)
    {

        Firebase fbs1 =  new Firebase("https://ad11.firebaseio.com/");
        Firebase fbs2 = fbs1.child("cars").child(car_number);
        fbs2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               my_car = dataSnapshot.getValue(car.class);
                btn_show.setText(my_car.toString());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }
    public void show_final(View v1)
    {
        btn_show.setText(my_car.toString());
    }
    public  void getKeys()
    {
        Firebase fbs1 =  new Firebase("https://ad11.firebaseio.com/");
        Firebase fbs2 = fbs1.child("cars");

        fbs2.limitToLast(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot dsp:dataSnapshot.getChildren())
                {
                    Log.i("a4"," key is "+dsp.getKey());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
