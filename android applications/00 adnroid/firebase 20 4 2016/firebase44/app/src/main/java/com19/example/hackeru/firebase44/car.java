package com19.example.hackeru.firebase44;

/**
 * Created by hackeru on 4/20/2016.
 */
public class car {
    private String carNumber;
    private  String company;
    private  int price;

    @Override
    public String toString() {
        return "car{" +
                "carNumber='" + carNumber + '\'' +
                ", company='" + company + '\'' +
                ", price=" + price +
                '}';
    }

    public car()
    {

    }


    public car(String carNumber, String company, int price) {
        this.carNumber = carNumber;
        this.company = company;
        this.price = price;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
