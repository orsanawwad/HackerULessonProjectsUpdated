package com19.example.hackeru.firebase44;

/**
 * Created by hackeru on 4/20/2016.
 */
public class messageClass {
    private  String owner;
    private  String message;

    public messageClass(String owner, String message) {
        this.owner = owner;
        this.message = message;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
