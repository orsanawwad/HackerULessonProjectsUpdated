package com19.example.hackeru.firebasechat1;

/**
 * Created by hackeru on 4/20/2016.
 */
public class massageClass {

    private String message;
    private  String owner;

    public massageClass()
    {

    }

    public massageClass(String message, String owner) {
        this.message = message;
        this.owner = owner;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
