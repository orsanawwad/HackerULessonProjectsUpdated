package com19.example.hackeru.firebasechat1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hackeru on 4/20/2016.
 */
public class messageAdapter extends ArrayAdapter<String> {
    public messageAdapter(Context context ,List<String> objects) {
        super(context, 0,  objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        String str = getItem(position);

        if(convertView== null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.ist_layout,parent,false);

        }

        EditText ed1 = (EditText)convertView.findViewById(R.id.t1);
        ed1.setText(str);
        return convertView;
    }
}
