package com19.example.hackeru.firebasechat1;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import java.util.List;

/**
 * Created by hackeru on 4/20/2016.
 */
public class myMassageAddapter  extends ArrayAdapter<massageClass> {
    public myMassageAddapter(Context context, List<massageClass> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        massageClass m1 = getItem(position);

        if(convertView==null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.ist_layout,parent,false);
        }
        EditText ed1 = (EditText)convertView.findViewById(R.id.t1);
        if(m1.getOwner().equals(MainActivity.userNmae))
        {
            ed1.setBackgroundColor(Color.BLUE);
        }
        else
        {
            ed1.setBackgroundColor(Color.GREEN);
        }
ed1.setText(m1.getMessage());
        return convertView;
    }
}
