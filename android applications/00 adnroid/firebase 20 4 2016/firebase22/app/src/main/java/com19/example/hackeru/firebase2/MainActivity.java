package com19.example.hackeru.firebase2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int num =0;
    EditText ed1 ;
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Firebase.setAndroidContext(this);
        ed1 = (EditText)findViewById(R.id.ed1) ;
        b1 = (Button)findViewById(R.id.button) ;
        b1.setOnClickListener(this);
        Firebase fbs1 = new Firebase("https://ad11.firebaseio.com/");
        fbs1.setValue("hello");
        fbs1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 num++;
              //  b1.setText("num of changes "+num+"");
                String all= "";
                for(DataSnapshot ds:dataSnapshot.getChildren())
                {
                    String str = ds.getValue(String.class);
                    all += str+"\n";

                }
                b1.setText(all);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    void my_click(View v1)
    {


    }

    @Override
    public void onClick(View v) {
        Firebase fbs1 = new Firebase("https://ad11.firebaseio.com/");
       // fbs1.setValue(ed1.getText().toString());
        fbs1.push().setValue(ed1.getText().toString());
    }
}
