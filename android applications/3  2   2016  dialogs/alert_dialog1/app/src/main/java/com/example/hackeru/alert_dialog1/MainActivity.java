package com.example.hackeru.alert_dialog1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button b1 = null;
    Context cn= this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
b1 = (Button)findViewById(R.id.button);
        b1.setOnClickListener(this);

        AlertDialog.Builder dialog_builder = new AlertDialog.Builder(cn);
        dialog_builder.setMessage("hello i am ur firts alert");
        dialog_builder.setTitle("alert title");

        dialog_builder.setPositiveButton("YES ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                b1.setText("the user choice yes");
            }
        }).setNegativeButton("NO TNX", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                b1.setText("the user choice no");
            }
        });
        AlertDialog dialog = dialog_builder.create();

        dialog.show();
    }

    @Override
    public void onClick(View v) {



    }
}
