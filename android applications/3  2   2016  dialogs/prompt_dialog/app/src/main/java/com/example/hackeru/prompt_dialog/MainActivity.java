package com.example.hackeru.prompt_dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    Button b1,b2;
    EditText ed1;
    Context cn1 = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
b1 = (Button)findViewById(R.id.btn1_main);
        LayoutInflater inf1 = LayoutInflater.from(cn1);
        View my_view = inf1.inflate(R.layout.prompt_layout,null);
        b2 = (Button)my_view.findViewById(R.id.btn_1);
        ed1 = (EditText)my_view.findViewById(R.id.editText);
        AlertDialog.Builder my_builder = new  AlertDialog.Builder(this);
        my_builder.setView(my_view);
        my_builder.setMessage("my mesasge");
        my_builder.setPositiveButton("get data", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String str ;
                str = ed1.getText().toString();
                b1.setText(str);

            }
        }).setNegativeButton(" no plz ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                b1.setText("no data");
            }
        });

        AlertDialog a1 = my_builder.create();
        a1.show();

        //inf1 = LayoutInflater.from(this);


    }
}
