package inner_classes1;

import inner_classes1.outer_class.inner_class;

public class my_main_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String str="123";
		
		outer_class oter_object = new outer_class();
		outer_class.inner_class inner_object = oter_object.new inner_class(100, "abc");
		outer_class outer1 = new outer_class("", inner_object);
		
		// error
		//outer_class.inner_class in12 = new outer_class.inner_class();
	}

}
