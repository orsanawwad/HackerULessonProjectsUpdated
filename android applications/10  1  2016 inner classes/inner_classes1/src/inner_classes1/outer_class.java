package inner_classes1;

public class outer_class {
	
	private String str;
	
	private inner_class my_inner_class_object;
	
	
	public outer_class()
	{
		
	}
	
	
	
	
	
	
	public outer_class(String str, inner_class my_inner_class_object) {
		super();
		this.str = str;
		this.my_inner_class_object = my_inner_class_object;
	}



	public class inner_class
	{
		private int num;
		private String str1;
		
		
		
		public int getNum() {
			return num;
		}
		public void setNum(int num) {
			this.num = num;
		}
		public String getStr1() {
			return str1;
		}
		public void setStr1(String str1) {
			this.str1 = str1;
		}
		public inner_class(int num, String str1) {
			super();
			this.num = num;
			this.str1 = str1;
		}
		
		
	}

}
