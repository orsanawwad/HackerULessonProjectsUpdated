package com.example.hackeru.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class second_fargment extends Fragment {

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    private String msg;
    public second_fargment() {
        // Required empty public constructor
    }

Button b1 ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View V1 = inflater.inflate(R.layout.fragment_second_fargment,container,false);
        b1 = (Button)V1.findViewById(R.id.b1_fr2);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        return inflater.inflate(R.layout.fragment_second_fargment, container, false);
    }

    class  my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            b1.setText(msg);
        }
    }


}
