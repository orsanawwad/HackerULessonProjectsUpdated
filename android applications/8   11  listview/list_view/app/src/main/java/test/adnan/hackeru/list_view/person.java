package test.adnan.hackeru.list_view;

/**
 * Created by hackeru on 08/11/2015.
 */
public class person  {
    private  String name;
    private  String id;

    public person(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "name:"+this.name+" id "+this.id;
    }
}
