package com.example.adnan.myapplication;

import android.support.annotation.IntegerRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;
    public List<Integer> l1 = new ArrayList<Integer>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.show_1);
        b2 = (Button)findViewById(R.id.button2);

    }

   public  void my_click(View v1)
    {

        switch (v1.getId()) {
            case  R.id.show_1:
            android.app.FragmentManager fmng = getFragmentManager();
            android.app.FragmentTransaction tr1 = fmng.beginTransaction();
            BlankFragment bl = new BlankFragment();
            tr1.add(R.id.rl1, bl);
            tr1.commit();
                break;
            case R.id.button2:

                b1.setText("num of elements is "+l1.size());
                break;

        }


    }
}
