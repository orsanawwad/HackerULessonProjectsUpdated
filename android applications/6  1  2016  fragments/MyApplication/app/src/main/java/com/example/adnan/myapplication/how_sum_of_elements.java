package com.example.adnan.myapplication;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class how_sum_of_elements extends Fragment {



Button b1,b2;
    public how_sum_of_elements() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View my_view = inflater.inflate(R.layout.fragment_how_sum_of_elements,container,false);
        b1 = (Button)my_view.findViewById(R.id.show_sum_1);
        b2 = (Button)my_view.findViewById(R.id.show_sum_2);
my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        b2.setOnClickListener(m1);

        return my_view;
    }

    class my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.show_sum_1:
                    MainActivity mn1 = (MainActivity)getActivity();
                    List<Integer> list_ref ;
                    list_ref = mn1.l1;
                    int sum=0;
                    for(int i=0;i<list_ref.size();i++)
                        sum += list_ref.get(i);
                    b1.setText("sum of elemnts :"+sum);

                    break;

                case R.id.show_sum_2:

                    break;
            }
        }
    }

}
