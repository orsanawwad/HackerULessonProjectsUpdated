package com.example.adnan.cars_dfargments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class add_car extends Fragment {

Button b1;
    EditText ed_num,ed_price;
    public add_car() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View my_view;
        my_view = inflater.inflate(R.layout.fragment_add_car,container,false);
        b1 = (Button)my_view.findViewById(R.id.add);
        ed_num = (EditText)my_view.findViewById(R.id.car_number);
        ed_price = (EditText)my_view.findViewById(R.id.car_price);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);



        return my_view;
    }

    class  my_listner implements View.OnClickListener
    {


        @Override
        public void onClick(View v) {
            MainActivity mn1 =  (MainActivity) getActivity();
            car cr1;
            String number;
            int price;
            number = ed_num.getText().toString();
            price = Integer.parseInt(ed_price.getText().toString());
            cr1= new car(number,price);
            mn1.l1.add(cr1);
        }
    }

}
