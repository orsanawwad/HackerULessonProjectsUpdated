package com.example.adnan.phone_call;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button B1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void my_click(View v1) {


        switch (v1.getId())
        {
            case R.id.phone_call:
                try {
                    Intent in1 = new Intent(Intent.ACTION_CALL, Uri.parse("tel:0528768244"));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(in1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case R.id.send_sms:



               sendEmail();


                try {
                    String phone_number="0528768244";
                    String msg = "hello from ersan application";


                    SmsManager manger = SmsManager.getDefault();
                    manger.sendTextMessage(phone_number,null,msg,null,null);
                } catch (Exception e) {
                }


                break;
        }


    }



    public void send_mail_app()
    {
        Intent in1 = new Intent(Intent.ACTION_SEND);
        String to,msg,cc,subject;
        to="adnan.fast@gmail.com";
        cc = "ad@gmail.com";
        msg ="hello from android application";
        subject = "my subject";
        in1.setData(Uri.parse("mailto:"));
        in1.setType("text/plain");
        in1.putExtra(Intent.EXTRA_EMAIL,to);
        in1.putExtra(Intent.EXTRA_CC,cc);
        in1.putExtra(Intent.EXTRA_SUBJECT,subject);
        in1.putExtra(Intent.EXTRA_TEXT, msg);
        startActivity(Intent.createChooser(in1,""));
        finish();


    }









    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {"adnan.fast@gmail.com"};
        String[] CC =
                {"adnan.fast@gmail.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

     //   emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");


        try {
            startActivity(Intent.createChooser(emailIntent, ""));
            finish();
            //     Log.i("Finished sending email...", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


}
