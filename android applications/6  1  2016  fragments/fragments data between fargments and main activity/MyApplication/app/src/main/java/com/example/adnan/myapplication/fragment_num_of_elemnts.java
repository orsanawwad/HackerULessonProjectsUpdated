package com.example.adnan.myapplication;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_num_of_elemnts extends Fragment {


    Button  b1;
    public fragment_num_of_elemnts() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  my_view = inflater.inflate(R.layout.fragment_fragment_num_of_elemnts,container,false);

        b1 = (Button)my_view.findViewById(R.id.b7);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        return my_view;
    }
class my_listner implements  View.OnClickListener
{

    @Override
    public void onClick(View v) {


        MainActivity m1 = (MainActivity)getActivity();
        int res=0;
        res = m1.l1.size();
        b1.setText("num of elemnts is :"+res);


    }
}
}
