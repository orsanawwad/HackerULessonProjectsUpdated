package krowges.dynamiccalc;

import android.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

public class MainActivity extends ActionBarActivity {
    LinearLayout lay1=null;
    Button n1,n2,n3,n4,n5,n6,n7,n8,n9,n0;
    Button plus,minus,mult,div;
    EditText numberHold;
    int i =0;
    TableLayout t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t1 = new TableLayout(this);
        lay1 = new LinearLayout(this);
        LinearLayout.LayoutParams lpr = new LinearLayout.LayoutParams( ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        t1.setLayoutParams(lpr);
        lay1.setLayoutParams(lpr);
        n1 = new Button(this);
        n2 = new Button(this);
        n3 = new Button(this);
        n4 = new Button(this);
        n5 = new Button(this);
        n6 = new Button(this);
        n7 = new Button(this);
        n8 = new Button(this);
        n9 = new Button(this);
        n0 = new Button(this);
        plus = new Button(this);
        minus = new Button(this);
        mult = new Button(this);
        div = new Button(this);
        numberHold = new EditText(this);
        n1.setText("1");
        n2.setText("2");
        n3.setText("3");
        n4.setText("4");
        n5.setText("5");
        n6.setText("6");
        n7.setText("7");
        n8.setText("8");
        n9.setText("9");
        n0.setText("0");
        n0.setId(i);
        i++;
        n1.setId(i);
        i++;
        n2.setId(i);
        i++;
        n3.setId(i);
        i++;
        n4.setId(i);
        i++;
        n5.setId(i);
        i++;
        n6.setId(i);
        i++;
        n7.setId(i);
        i++;
        n8.setId(i);
        i++;
        n9.setId(i);

        Digits d = new Digits();
        n1.setOnClickListener(d);
        n2.setOnClickListener(d);
        n3.setOnClickListener(d);
        n4.setOnClickListener(d);
        n5.setOnClickListener(d);
        n6.setOnClickListener(d);
        n7.setOnClickListener(d);
        n8.setOnClickListener(d);
        n9.setOnClickListener(d);
        n0.setOnClickListener(d);
        plus.setText("+");
        minus.setText("-");
        mult.setText("*");
        div.setText("/");
        TableRow trk = new TableRow(this);
        trk.addView(numberHold);
        t1.addView(trk);
        TableRow tr = new TableRow(this);
        tr.addView(n1);
        tr.addView(n2);
        tr.addView(n3);
        t1.addView(tr);
        TableRow tr2 = new TableRow(this);
        tr2.addView(n4);
        tr2.addView(n5);
        tr2.addView(n6);
        t1.addView(tr2);
        TableRow tr3 = new TableRow(this);
        tr3.addView(n7);
        tr3.addView(n8);
        tr3.addView(n9);
        t1.addView(tr3);
        TableRow tr4 = new TableRow(this);
        tr4.addView(n0);
        t1.addView(tr4);

        TableRow tr5 = new TableRow(this);
        tr5.addView(plus);
        tr5.addView(minus);
        tr5.addView(mult);
        t1.addView(tr5);
        TableRow tr6 = new TableRow(this);
        tr6.addView(div);
        t1.addView(tr6);
        lay1.addView(t1);
        setContentView(lay1);

    }
    public class Digits implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case 0:

                    numberHold.setText(numberHold.getText()+"0");
                    break;
                case 1:
                    numberHold.setText(numberHold.getText()+"1");
                    break;
                case 2:
                    numberHold.setText(numberHold.getText()+"2");
                    break;
            }
        }
    }
}
