package test.adnan.hackeru.calc2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class my_first_app extends AppCompatActivity {

    Button add,sub,men,devide,ressult;
    EditText ed1,ed2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_first_app);
        add =  (Button) findViewById(R.id.button);
        sub =  (Button) findViewById(R.id.button2);
        men =  (Button) findViewById(R.id.button3);
        devide =  (Button) findViewById(R.id.button4);
        ressult =  (Button) findViewById(R.id.button5);


        ed1 = (EditText)findViewById(R.id.editText);
        ed2 = (EditText)findViewById(R.id.editText2);
        my_listnre1 m1 = new my_listnre1();
        my_listnre2 m2 = new my_listnre2();
        my_listnre3 m3 = new my_listnre3();
        my_listnre4 m4 = new my_listnre4();
        add.setOnClickListener(m1);
        sub.setOnClickListener(m2);
        men.setOnClickListener(m3);
        devide.setOnClickListener(m4);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_first_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class  my_listnre1 implements View.OnClickListener
    {


        @Override
        public void onClick(View v) {

            String s1,s2;
            int n1,n2,n3;
            s1 = ed1.getText().toString();
            s2 = ed2.getText().toString();
            n1 = Integer.parseInt(s1);
            n2 = Integer.parseInt(s2);
            n3 = n1+n2;
            ressult.setText(n3+"");
           // add.setText("u clicked add button");
        }
    }
    public class  my_listnre2 implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            String s1,s2;
            int n1,n2,n3;
            s1 = ed1.getText().toString();
            s2 = ed2.getText().toString();
            n1 = Integer.parseInt(s1);
            n2 = Integer.parseInt(s2);
            n3 = n1-n2;
            ressult.setText(n3+"");
         //   sub.setText("u clicked add button");
        }
    }
    public class  my_listnre3 implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {


            String s1,s2;
            int n1,n2,n3;
            s1 = ed1.getText().toString();
            s2 = ed2.getText().toString();
            n1 = Integer.parseInt(s1);
            n2 = Integer.parseInt(s2);
            n3 = n1*n2;
            ressult.setText(n3+"");
          //  men.setText("u clicked add button");
        }
    }
    public class  my_listnre4 implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            String s1,s2;
            int n1,n2,n3;
            s1 = ed1.getText().toString();
            s2 = ed2.getText().toString();
            n1 = Integer.parseInt(s1);
            n2 = Integer.parseInt(s2);
            n3 = n1/n2;
            ressult.setText(n3+"");
          //  devide.setText("u clicked add button");
        }
    }
}
