package test.adnan.hackeru.localization_a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    String [][]user_interface= new String[10][20];
    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = (Button)findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);

        b1.setText(R.string.save);
        b2.setText(R.string.show);
      //  user_interface[0] = new String[9];
        user_interface[0][0]="enter";
       // user_interface[1] = new String[9];
        user_interface[1][0]="הכנס";


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
if(id==R.id.it1)
{
  b1.setText("item 1 ");
}
        else
if(id==R.id.it2)
{
    b1.setText("item 2 ");
}
        else
if(id==R.id.it3)
{
    b1.setText("item 3 ");
}
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
