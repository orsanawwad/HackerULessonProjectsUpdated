package com.example.hackeru.nikitacheck;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.app.Activity;
import android.widget.Toast;

public class MainActivity extends Activity {

    Button add,show,sale,num_of_products;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add = (Button)findViewById(R.id.add);
        show = (Button)findViewById(R.id.show);
        sale = (Button)findViewById(R.id.sale);
        num_of_products = (Button)findViewById(R.id.count);
        my_listner m1 = new my_listner();
        add.setOnClickListener(m1);
        show.setOnClickListener(m1);
        sale.setOnClickListener(m1);
        num_of_products.setOnClickListener(m1);
        Toast.makeText(this,static_members.l1.size()+"",Toast.LENGTH_SHORT).show();

    }


    public  class my_listner implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {

            switch ((v.getId()))
            {
                case R.id.add:
                    Intent in1 = new Intent(MainActivity.this,add_new_item.class);
                    startActivity(in1);

                    break;
                case R.id.show:

                    Intent in2 = new Intent(MainActivity.this,show_all_items.class);
                    startActivity(in2);

                    break;
                case R.id.sale:

                    Intent in3 = new Intent(MainActivity.this,sale_items.class);
                    startActivity(in3);
                    break;
                case R.id.count:
                    Toast.makeText(MainActivity.this,static_members.l1.size()+"",Toast.LENGTH_SHORT).show();


                    break;
            }
        }
    }
}
