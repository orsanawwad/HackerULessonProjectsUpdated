package com.example.hackeru.nikitacheck;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class sale_items extends AppCompatActivity {


    EditText num_of_prod,pro_code;
    Button save;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_items);

        num_of_prod = (EditText)findViewById(R.id.num);
        pro_code = (EditText)findViewById(R.id.code);
        save = (Button)findViewById(R.id.button2);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num;
                String code;
                num =  Integer.parseInt(num_of_prod.getText().toString());
                code = pro_code.getText().toString();
                for (product p1:static_members.l1)
                {
                    if(p1.getCode().equals(code))
                    {
                        int total ;
                        total = p1.sale(num);
                        if(total<0)
                            save.setText("אין מספיק במלאי");
                        else
                        save.setText(" u have to pay "+total);
                    }
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sale_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
