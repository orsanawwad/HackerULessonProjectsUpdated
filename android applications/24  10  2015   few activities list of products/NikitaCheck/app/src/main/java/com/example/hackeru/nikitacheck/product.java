package com.example.hackeru.nikitacheck;

/**
 * Created by hackeru on 25/10/2015.
 */
public class product {

    private String name;
    private String code;
    private int  price;
    private int  quan;

    @Override
    public String toString() {
        return this.name+" "+this.code+" "+this.price+"  "+this+quan;
    }

    // if the function return -1 the action was stoped
    public  int sale(int num)
    {
        if(num<=quan)
        {
            quan -= num;
            return this.price*num;
        }
        return  -1;
    }


    public String getName() {
        return name;
    }

    public product(String name, String code, int price, int quan) {
        this.name = name;
        this.code = code;
        this.price = price;
        this.quan = quan;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuan() {
        return quan;
    }

    public void setQuan(int quan) {
        this.quan = quan;
    }
}
