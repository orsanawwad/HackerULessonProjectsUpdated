package test.adnan.hackeru.image_effects;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;
    ImageView im1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        im1 = (ImageView)findViewById(R.id.imageView);
        im1.setImageResource(R.drawable.p2);
        b1 = (Button)findViewById(R.id.button);
        b2 = (Button)findViewById(R.id.button2);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        b2.setOnClickListener(m1);

    }
public Bitmap effect_on_bitmap(Bitmap bmp1)
{
    Bitmap second = null;
    second = Bitmap.createBitmap(bmp1.getWidth(),bmp1.getHeight(),bmp1.getConfig());
    for(int x=0;x<bmp1.getWidth();x++)
        for (int y=0;y<bmp1.getHeight();y++)
        {
            int pixel;
            pixel = bmp1.getPixel(x, y);
            int r,g,b,a;
            r = Color.red(pixel);
            g = Color.green(pixel);
            b = Color.blue(pixel);
            a = Color.alpha(pixel);
            r = 255-r;
            g = 255-g;
            b =255-b;
            second.setPixel(x, y, Color.argb(a, r, g, b));
        }

    return  second;
}
    public Bitmap get_part_of_pic(Bitmap first)
    {
        Bitmap part_of;
        part_of = Bitmap.createBitmap(first.getWidth(),first.getHeight(),first.getConfig());
        for (int x=0;x<first.getHeight()/2;x++)
            for (int y=0;y<first.getHeight()/2;y++)
            {
                int pixel;
                int r,g,b,a;
                pixel = first.getPixel(x,y);
                r = Color.red(pixel);
                g = Color.green(pixel);
                b = Color.blue(pixel);
                a = Color.alpha(pixel);
                part_of.setPixel(x,y,Color.argb(a,r,g,b));
            }
        return  part_of;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public Bitmap get_bitmap_from_iamgeview(ImageView IM)
    {
        Bitmap res=null;
        BitmapDrawable bmdr = (BitmapDrawable) IM.getDrawable();
        res = bmdr.getBitmap();
        return  res;

    }

    class my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {


            switch (v.getId())
            {
                case R.id.button:
                    try {
                        Bitmap my_bmp = get_bitmap_from_iamgeview(im1);

                        Bitmap chnged_bmp;
                        chnged_bmp = effect_on_bitmap(my_bmp);
                        im1.setImageBitmap(chnged_bmp);
                    } catch (Exception e) {
                        Log.i("am11",e.getMessage());
                    }
                    break;
                case R.id.button2:
                    BitmapDrawable bmdr1 = (BitmapDrawable) im1.getDrawable();
                    Bitmap bp= bmdr1.getBitmap();
                    Bitmap my_part = get_part_of_pic(bp);
                    im1.setImageBitmap(my_part);
                    break;
            }


        }
    }

}
