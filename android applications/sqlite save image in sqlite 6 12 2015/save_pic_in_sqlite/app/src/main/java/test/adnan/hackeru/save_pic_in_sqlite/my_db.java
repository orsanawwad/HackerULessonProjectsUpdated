package test.adnan.hackeru.save_pic_in_sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hackeru on 06/12/2015.
 */
public class my_db extends SQLiteOpenHelper {
    public my_db(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query ;
        query = "create  table if not exists t1(code integer,image BLOB) ";
        db.execSQL(query);

    }
    public  void add_to_table(int code,byte []image)
    {
        String query;
        ContentValues cn1 = new ContentValues();
        cn1.put("code",code);
        cn1.put("image",image);
        SQLiteDatabase db = getWritableDatabase();
        db.insert("t1", null, cn1);
    }
    public byte[] get_image_by_code(int code)
    {
        byte []res=null;
        String query = "select * from t1 where code = "+code;
        Cursor cr;
        SQLiteDatabase db = getWritableDatabase();
        cr = db.rawQuery(query,null);
        if(cr.moveToFirst())
        {
            do {


               res = cr.getBlob(1);
            } while (cr.moveToNext());
        }

        return  res;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
