/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.List;


public class MainActivity extends ActionBarActivity {

  String name;
  byte []pic11;
  Button save,get;
  ImageView im1,im2;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ParseAnalytics.trackAppOpenedInBackground(getIntent());

    save = (Button)findViewById(R.id.save);
    get =(Button)findViewById(R.id.get  );


    im1 = (ImageView)findViewById(R.id.im1);
im1.setVisibility(View.INVISIBLE);
    im2 = (ImageView)findViewById(R.id.im2);
    im1.setImageResource(R.drawable.p1);


    my_listner m1 = new my_listner();
    save.setOnClickListener(m1);
    get.setOnClickListener(m1);



  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }




  public static byte[] getBytes(Bitmap bitmap) {
    ByteArrayOutputStream stream = new ByteArrayOutputStream();
    bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
    return stream.toByteArray();
  }

  // convert from byte array to bitmap
  public static Bitmap getImage(byte[] image) {
    return BitmapFactory.decodeByteArray(image, 0, image.length);
  }

  class  my_listner implements View.OnClickListener
  {

    @Override
    public void onClick(View v) {

      switch (v.getId())
      {
        case R.id.save:

          BitmapDrawable bmdr = (BitmapDrawable) im1.getDrawable();


          Bitmap bmp1 = bmdr.getBitmap();
          byte []pic1 = getBytes(bmp1);



          ParseObject testObject = new ParseObject("table_iamges");
          testObject.put("animal", "cow");
          testObject.put("pic",pic1);
          testObject.saveInBackground();
          break;
        case R.id.get :

          ParseQuery<ParseObject> query = ParseQuery.getQuery("table_iamges");
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> objects, com.parse.ParseException e) {
        for (int i=0;i<objects.size();i++)
        {
          name = objects.get(i).getString("animal");
          pic11 = objects.get(i).getBytes("pic");
        }
        try {
          Bitmap bmp12 = getImage(pic11);
          im2.setImageBitmap(bmp12);
          get.setText(name);
        } catch (Exception e1) {
          e1.printStackTrace();
        }

      }
    });


          break;
      }

    }
  }


}
