package com.example.android.ontoucheventt;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by android on 28/10/2015.
 */
public class my_draw extends View {

    public my_draw(Context context) {
        super(context);


    }
float cx=0,cy=0;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p1 = new Paint();
        p1.setColor(Color.RED);
        canvas.drawCircle(cx,cy,50,p1);

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //mGestureDetector.onTouchEvent(event);

        int action = event.getActionMasked();

        switch (action) {

            case MotionEvent.ACTION_DOWN:
                cx = event.getX();
                cy = event.getY();
                break;

            case MotionEvent.ACTION_MOVE:

                break;

            case MotionEvent.ACTION_UP:

                break;

            case MotionEvent.ACTION_CANCEL:

                break;

            case MotionEvent.ACTION_OUTSIDE:

                break;
        }
    invalidate();
        return true;
    }

}
