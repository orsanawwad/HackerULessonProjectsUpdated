package com.example.adnan.json_with_json_array;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button b1 =null;
    String all_text;
    EditText ed1,ed2;
    person per1;
    Button create_person;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        create_person = (Button)findViewById(R.id.create_person);
        ed1 = (EditText)findViewById(R.id.ed_comp);
        ed2 = (EditText)findViewById(R.id.ed_phone);

        b1 = (Button)findViewById(R.id.show_with_list);
    }
    public void  my_click(View v1)
    {
        switch (v1.getId())
        {
            case R.id.create_person:
                person tmp = new person();

                person.Adress adr1 = tmp.new Adress("haifa","nveem",20);
                per1 = new person("avi","1234",adr1);
                String res = create_json_from_person_no_list(per1);
                Log.i("a11",res);

                break;
            case R.id.add_phone:
                String phone_number,comp_name;
                phone_number = ed2.getText().toString();
                comp_name = ed1.getText().toString();
                person tmp2 = new person();
                person.Phone ph1 = tmp2.new Phone(comp_name,phone_number);
                per1.getList_of_phones().add(ph1);
                ed1.setText("");
                ed2.setText("");


                break;

            case R.id.show_with_list:
          String res1=      create_json_from_person_with_list(per1);
                all_text = res1;
                Log.i("a13",res1);

                break;

            case R.id.show_person_from_json:
                person waw_person;
                waw_person = convert_string_json_to_person(all_text);
                b1.setText(waw_person+"");

                break;

        }



    }

    public String create_json_from_person_no_list(person my_person)
    {
        String all="";
        JSONObject json_outer= new JSONObject();


        try {
            json_outer.put("name",my_person.getName());
            json_outer.put("id",my_person.getId());
            JSONObject inner = new JSONObject();
            inner.put("street",my_person.getPer_adress().getStreet());
            inner.put("city",my_person.getPer_adress().getCity());
            inner.put("street_num",my_person.getPer_adress().getStreet_num());
            json_outer.put("adress",inner);
            all = json_outer.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  all;
    }
    public String create_json_from_person_with_list(person my_person)
    {
        String all="";
        JSONObject json_outer= new JSONObject();


        try {
            json_outer.put("name",my_person.getName());
            json_outer.put("id",my_person.getId());
            JSONObject inner = new JSONObject();
            inner.put("street",my_person.getPer_adress().getStreet());
            inner.put("city",my_person.getPer_adress().getCity());
            inner.put("street_num",my_person.getPer_adress().getStreet_num());
            json_outer.put("adress",inner);


            JSONArray my_arry = new JSONArray();
            for(int i=0;i<my_person.getList_of_phones().size();i++)
            {
                JSONObject json11 = new JSONObject();
                person.Phone ph11;
                ph11 = my_person.getList_of_phones().get(i);
             //   json11.put("comp",my_person.getList_of_phones().get(i).getComp());
                json11.put("comp",ph11.getComp());
                json11.put("number",ph11.getNumber());
                my_arry.put(json11);

            }
            json_outer.put("phones_list",my_arry);

            all = json_outer.toString();



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  all;
    }

    public person convert_string_json_to_person(String str)
    {
        String name,id,city,street;
        int street_num;

        person.Phone phone1;

        List<person.Phone> my_list= new ArrayList<person.Phone>();
        person result= null;
        JSONObject outer;
        JSONObject inner;
        JSONArray my_arr;
        JSONObject tmps;
        try {
            outer = new JSONObject(str);
            name = outer.getString("name");
            id = outer.getString("id");
            inner = outer.getJSONObject("adress");
            city = inner.getString("city");
            street = inner.getString("street");
            street_num = inner.getInt("street_num");
            person my_person= new person();
            person.Adress my_adr = my_person.new Adress(city,street,street_num);
            my_person.setPer_adress(my_adr);
            my_person.setName(name);
            my_person.setId(id);

            JSONArray my_json_array = outer.getJSONArray("phones_list");
            for (int i=0;i<my_json_array.length();i++)
            {
                JSONObject job = my_json_array.getJSONObject(i);
                String comp,number;
                comp = job.getString("comp");
                number = job.getString("number");
                person.Phone phon11 = my_person.new Phone(comp,number);
                my_list.add(phon11);

            }
            my_person.setList_of_phones(my_list);

result = my_person;


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return  result;
    }
}
