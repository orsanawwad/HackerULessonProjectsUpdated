package com.example.adnan.json_with_json_array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adnan on 13/01/2016.
 */
public class person {

    private  String name;
    private String id;
    private Adress per_adress;
    private List<Phone> list_of_phones= new ArrayList<Phone>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Adress getPer_adress() {
        return per_adress;
    }

    public void setPer_adress(Adress per_adress) {
        this.per_adress = per_adress;
    }

    public List<Phone> getList_of_phones() {
        return list_of_phones;
    }

    public void setList_of_phones(List<Phone> list_of_phones) {
        this.list_of_phones = list_of_phones;
    }

    public  person()
    {

    }

    @Override
    public String toString() {
        return "person{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", per_adress=" + per_adress +
                ", list_of_phones=" + list_of_phones +
                '}';
    }

    public person(String name, String id, Adress per_adress, List<Phone> list_of_phones) {
        this.name = name;
        this.id = id;
        this.per_adress = per_adress;
        this.list_of_phones = list_of_phones;
    }

    public person(String name, String id, Adress per_adress) {
        this.name = name;
        this.id = id;
        this.per_adress = per_adress;
    }

    public  void  add_phone(Phone p1)
    {
        this.list_of_phones.add(p1);
    }


    public    class Adress
 {
     private String city;
     private  String street;
     private  int street_num;

     @Override
     public String toString() {
         return "Adress{" +
                 "city='" + city + '\'' +
                 ", street='" + street + '\'' +
                 ", street_num=" + street_num +
                 '}';
     }

     public Adress(String city, String street, int street_num) {
         this.city = city;
         this.street = street;
         this.street_num = street_num;
     }

     public String getCity() {
         return city;
     }

     public void setCity(String city) {
         this.city = city;
     }

     public String getStreet() {
         return street;
     }

     public void setStreet(String street) {
         this.street = street;
     }

     public int getStreet_num() {
         return street_num;
     }

     public void setStreet_num(int street_num) {
         this.street_num = street_num;
     }
 }
    public  class Phone
    {
        private String comp;
        private String number;

        @Override
        public String toString() {
            return "Phone{" +
                    "comp='" + comp + '\'' +
                    ", number='" + number + '\'' +
                    '}';
        }

        public String getComp() {
            return comp;
        }

        public void setComp(String comp) {
            this.comp = comp;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public Phone(String comp, String number) {
            this.comp = comp;
            this.number = number;
        }
    }
}
