package test.adnan.hackeru.graphics_with_animation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.view.View;

/**
 * Created by hackeru on 28/10/2015.
 */
public class myDraw extends View {


    public myDraw(Context context) {
        super(context);

        my_task m1 = new my_task() ;
        m1.execute(200,50,100,600);
    }

    int cx=0,cy=0;
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p1 = new Paint();
        p1.setColor(Color.GREEN);
        canvas.drawCircle(0, 200, 50, p1);

        canvas.drawCircle(cx,cy,50,p1);
    }

    public class my_task extends AsyncTask<Integer,Integer,Integer>
    {
        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

          invalidate();

        }

        @Override
        protected Integer doInBackground(Integer... values) {

            int speed,max_x;
            speed = values[0];
            cx = values[1];
            cy = values[2];
            max_x = values[3];
            while(cx<max_x)
            {
                cx+=10;
                cy+=5;

                try {
                    Thread.sleep(speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                publishProgress(cx);
            }
            return null;
        }
    }

}
