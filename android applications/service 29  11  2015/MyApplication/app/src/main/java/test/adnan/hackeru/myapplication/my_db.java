package test.adnan.hackeru.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hackeru on 29/11/2015.
 */
public class my_db extends SQLiteOpenHelper {
    public my_db(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "create table if not exists t1( num  integer)";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public  void add_num(int number)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cn = new ContentValues();
        cn.put("num",number);
        db.insert("t1",null,cn);


    }

    public String get_all()
    {
        SQLiteDatabase db = getWritableDatabase();
        String all ="";
        Cursor cr;

        String query;
        query = "select * from t1";
        cr = db.rawQuery(query,null);
        if(cr.moveToFirst())
        {
           do
            {
                all += cr.getInt(0)+"\n";
            } while (cr.moveToNext());
        }
        return all;
    }





}
