package com.example.hackeru.pager2;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    ViewPager vpgr1;
    List<person> my_persons_list = new ArrayList<person>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vpgr1 = (ViewPager)findViewById(R.id.vpg1);
        my_pager2 pager_ma = new my_pager2(getSupportFragmentManager(), create_fragments());
        vpgr1.setAdapter(pager_ma);
    }

    public List<person> getMy_persons_list() {
        return my_persons_list;
    }

    public void setMy_persons_list(List<person> my_persons_list) {
        this.my_persons_list = my_persons_list;
    }

    public List<Fragment> create_fragments()
    {
        List<Fragment> l1 = new ArrayList<Fragment>();
        add_person_fragment a1 = new add_person_fragment();
        l1.add(a1);

        show_all_fragment sh1 = new show_all_fragment();
        l1.add(sh1);

        search_fragment shf1 = new search_fragment();
        l1.add(shf1);

        return  l1;
    }

}
