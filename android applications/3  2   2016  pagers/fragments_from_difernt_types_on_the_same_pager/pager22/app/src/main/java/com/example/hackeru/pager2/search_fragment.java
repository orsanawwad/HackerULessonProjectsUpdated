package com.example.hackeru.pager2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class search_fragment extends Fragment{


    Button b1;
    EditText ed1;
    public search_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View my_view= inflater.inflate(R.layout.fragment_search_fragment,container,false);
        b1 = (Button)my_view.findViewById(R.id.btn_fr_search);
        ed1 = (EditText)my_view.findViewById(R.id.editText);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity m1 = (MainActivity)getActivity();
                List<person> my_list = m1.getMy_persons_list();
                String id;
                id = ed1.getText().toString().trim();
                for(int i=0;i<my_list.size();i++)
                    if(my_list.get(i).getId().equals(id)) {
                        b1.setText(my_list.get(i).getName());
return;
                    }

                        b1.setText("not found ");


            }
        });


        return my_view;
    }

}
