package com.example.hackeru.pager2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by hackeru on 2/3/2016.
 */
public class my_pager extends FragmentPagerAdapter {

    List<Fragment> l1;
    List<Fragment> l2;
    public my_pager(FragmentManager fm,List<Fragment> l1,List<Fragment> l2) {
        super(fm);
        this.l1 = l1;
        this.l2 = l2;
    }

    @Override
    public Fragment getItem(int position) {
        if(position<l1.size())
        return l1.get(position);
        else {
            int pos2;
            pos2 = position- l1.size();
            return l2.get(pos2);

        }
    }

    @Override
    public int getCount() {
        return l1.size()+l2.size();
    }
}
