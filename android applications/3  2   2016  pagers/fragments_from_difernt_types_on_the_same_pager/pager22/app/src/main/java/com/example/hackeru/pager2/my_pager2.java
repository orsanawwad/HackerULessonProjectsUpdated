package com.example.hackeru.pager2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by hackeru on 2/3/2016.
 */
public class my_pager2 extends FragmentPagerAdapter {

    List<Fragment> l1;
    public my_pager2(FragmentManager fm,List<Fragment> l1) {
        super(fm);
        this.l1 = l1;
    }

    @Override
    public Fragment getItem(int position) {
        return l1.get(position);
    }

    @Override
    public int getCount() {
        return l1.size();
    }
}
