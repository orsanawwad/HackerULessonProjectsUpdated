package com.example.hackeru.pager1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class first_fragment extends Fragment {

    private  String me;

    public String getme() {
        return me;
    }

    public void setMe(String me) {
        this.me = me;
    }

    public first_fragment() {
        // Required empty public constructor
    }

Button b1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View my_view = inflater.inflate(R.layout.fragment_first_fragment,container,false);
        b1 = (Button)my_view.findViewById(R.id.b1_fr1);
        b1.setText("  all about me:  "+me);

        return my_view;
    }

}
