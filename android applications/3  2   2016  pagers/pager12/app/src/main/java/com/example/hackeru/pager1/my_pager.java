package com.example.hackeru.pager1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.List;

/**
 * Created by hackeru on 2/2/2016.
 */
public class my_pager extends FragmentPagerAdapter {

List<first_fragment> l1 ;
    public my_pager(FragmentManager fm,List<first_fragment> l1) {
        super(fm);
        this.l1 = l1;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i("a11",position+"");
        return l1.get(position);
    }

    @Override
    public int getCount() {
        return l1.size();
    }
}
