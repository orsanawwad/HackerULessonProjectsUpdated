package com.example.hackeru.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button bt1,bt2;
    EditText ed1;

    AlertDialog.Builder dialog_alert;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt1 = (Button) findViewById(R.id.bt1);
        bt2 = (Button) findViewById(R.id.bt2);


    }

        public void my_click (View v1) {

        switch (v1.getId()) {
            case R.id.bt1:

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage("i am massage from button number 1")
                    .setTitle("Title from Button number 1 ")
                    .setPositiveButton("positive", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(MainActivity.this, "O.k i am possitive", Toast.LENGTH_LONG).show();
                        }
                    })
                    .setNegativeButton("negative", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(MainActivity.this, " am negative", Toast.LENGTH_LONG).show();
                        }
                    }).setNeutralButton("Natural", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(MainActivity.this, "i am natural", Toast.LENGTH_LONG).show();
                }


            });
            AlertDialog dialog = builder.create();
            dialog.show();

                break;

            case R.id.bt2:
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("i am massage from button number 2")
                        .setTitle("Title from Button number 2")
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_LONG).show();
                            }
                        })
                        .setNegativeButton("no", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(MainActivity.this, "no", Toast.LENGTH_LONG).show();
                            }
                        }).setNeutralButton("naturalll", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "natur", Toast.LENGTH_LONG).show();
                    }
                });

                AlertDialog dialog1 = builder.create();
                dialog1.show();
        }
    }


    }



