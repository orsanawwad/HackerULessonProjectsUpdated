package com.example.hackeru.sharepreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button b1,b2;
    EditText ed1,ed2,ed3,ed4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.add);
        b2 = (Button)findViewById(R.id.show);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

        ed1 = (EditText)findViewById(R.id.ed_name);
        ed2 = (EditText)findViewById(R.id.ed_password);
        ed3 = (EditText)findViewById(R.id.ed_show);
        ed4 = (EditText)findViewById(R.id.ed_show_me);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.add:
                SharedPreferences sharep1 = this.getSharedPreferences("roi1", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor11 = sharep1.edit();
                editor11.putString(ed1.getText().toString(),ed2.getText().toString());
                editor11.commit();

                break;

            case R.id.show:
                SharedPreferences sharep2 = this.getSharedPreferences("roi1", Context.MODE_PRIVATE);
                String name, str;
                name = ed3.getText().toString();
                str = sharep2.getString(name,"not found");
                ed4.setText(str);

                break;




        }

    }


}
