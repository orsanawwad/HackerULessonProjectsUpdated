package com.example.hackeru.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.view.View;

import java.util.Random;

/**
 * Created by hackeru on 28/10/2015.
 */
public class my_draw extends View {
    float cx1=20,cy1=40,cx2=100,cy2=70;
    int width,height;
    int stepx1=1,stepy1=1,stepx2=1,stepy2=1;
    public my_draw(Context context,int width,int height) {
        super(context);

        this.width = width;
        this.height = height;
        my_task m1 = new my_task();
        m1.execute(10);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p1 = new Paint();
        p1.setColor(Color.RED);
        Paint p2 = new Paint();
        p2.setColor(Color.GREEN);
        canvas.drawCircle(cx1, cy1, 50, p1);

        canvas.drawCircle(cx2,cy2,50,p2);
    }

    public class my_task extends AsyncTask<Integer,Integer,Integer>
    {

        @Override
        protected Integer doInBackground(Integer... params) {


            Random r1 = new Random();
            int val;
            if(true)
            while(true)
            {

                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


                if(cx1>=width)
                    stepx1*=-1;
                if(cx1<=0)
                    stepx1*=-1;

                val = r1.nextInt(10);

                cx1+= val*stepx1;


                if(cx2>=width)
                    stepx2*=-1;
                if(cx2<=0)
                    stepx2*=-1;

                val = r1.nextInt(10);

                cx2+= val*stepx2;



                if(cy1>=height)
                    stepy1*=-1;
                if(cy1<=0)
                    stepy1*=-1;

                val = r1.nextInt(10);

                cy1+= val*stepy1;






                if(cy2>=height)
                    stepy2*=-1;
                if(cy2<=0)
                    stepy2*=-1;

                val = r1.nextInt(10);

                cy2+= val*stepy2;
                publishProgress();
            }


            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            invalidate();
        }
    }


}
