package test.adnan.hackeru.sqlite_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hackeru on 04/11/2015.
 */
public class my_db  extends SQLiteOpenHelper {
    public my_db(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "create table if not exists t1 (age integer,name text)";
        db.execSQL(query);


    }

    public  String get_all_names()
    {
        String query = "select * from t1 ";
        String res="",name1;

        Cursor cr;
        SQLiteDatabase db1 = getWritableDatabase();
        cr = db1.rawQuery(query,null);

        if(cr.moveToFirst())
        {


            do {

                name1 = cr.getString(1);
                res+=name1+" ";

            }while ((cr.moveToNext()));
        }

        return  res;

    }



    public String add_number(int x,String my_name)
    {
        try {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cn1 = new ContentValues();
            cn1.put("age",x);
            cn1.put("name",my_name);
            db.insert("t1", null, cn1);
            db.close();
        } catch (Exception e) {
       return  e.getMessage();
        }
return  "ok";

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
