package test.adnan.hackeru.collage_db;

/**
 * Created by hackeru on 04/11/2015.
 */
public class student {

    private String id;
    private  String name;
    private  int avrg;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvrg() {
        return avrg;
    }

    public void setAvrg(int avrg) {
        this.avrg = avrg;
    }

    public String getId() {
        return id;
    }

    public student(String id, String name, int avrg) {
        this.id = id;
        this.name = name;
        this.avrg = avrg;
    }


    @Override
    public String toString() {
        return "name "+this.name+" id "+this.id+" avrg "+this.avrg;
    }
}
