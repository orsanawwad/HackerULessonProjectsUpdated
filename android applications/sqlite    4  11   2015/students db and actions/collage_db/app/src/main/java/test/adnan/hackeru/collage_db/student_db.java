package test.adnan.hackeru.collage_db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hackeru on 04/11/2015.
 */
public class student_db extends SQLiteOpenHelper {
    public student_db(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query= "create table if not exists students(name text,id text ,avrg  integer) ";
        db.execSQL(query);

    }


    public  void add_new_student(student s1)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cn1 = new ContentValues();
        cn1.put("name",s1.getName());
        cn1.put("id",s1.getId());
        cn1.put("avrg",s1.getAvrg());
        db.insert("students", null, cn1);
        db.close();
    }



    public List<student>get_by_avrg(int avrg1,int direction)
    {

        String qury=null ;
        if(direction ==1)
            qury = "select * from students where avrg > "+avrg1;
        else
            if(direction==2)
                qury = "select * from students where avrg < "+avrg1;

        Cursor cr1;
        String name,id;
        int avrg;
        SQLiteDatabase db = getWritableDatabase();
        cr1 = db.rawQuery(qury,null);
        List<student> my_list = new ArrayList<student>();
        student s1;
        if(cr1.moveToFirst()) {
            do {

                name = cr1.getString(0);
                id = cr1.getString(1);
                avrg = cr1.getInt(2);

                s1 = new student(id, name, avrg);

                my_list.add(s1);
            } while (cr1.moveToNext());
        }

      return  my_list;


    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
