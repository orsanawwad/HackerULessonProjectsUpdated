package com.example.hackeru.client_server;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText ed_name,ed_age,ed_id;
    Button add,show_by_id,btn_show;
    AsyncHttpClient clinet1= new AsyncHttpClient();
    RequestParams params1 = new RequestParams();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add =(Button)findViewById(R.id.btn_add);
        show_by_id = (Button)findViewById(R.id.btn_find);
        btn_show = (Button)findViewById(R.id.btn_show);
        btn_show.setOnClickListener(this);
ed_name = (EditText)findViewById(R.id.ed_name);
        ed_id = (EditText)findViewById(R.id.ed_id);
        ed_age = (EditText)findViewById(R.id.ed_age);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btn_add:
                String name,id;
                int age;
                name = ed_name.getText().toString();
                id = ed_id.getText().toString();
                age = Integer.parseInt(ed_age.getText().toString());
                params1.put("name",name);
                params1.put("id",id);
                params1.put("age", age);
                clinet1.get("http://95.213.174.186/a1/home/test_add_person", params1, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        add.setText(" error code" + statusCode);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        add.setText(responseString + " status code " + statusCode);
                    }
                });
                break;


            case R.id.btn_find:

                String id1;
                id1 = ed_id.getText().toString();
                params1.put("id",id1);
                clinet1.get("http://95.213.174.186/a1/home/tets_get_person_by_id", params1, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        show_by_id.setText(responseString + " error code" + statusCode);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {

                        show_by_id.setText(responseString);
                    }
                });


                break;

            case  R.id.btn_show:
                clinet1.get("http://95.213.174.186/a1/home/get_all_personsf", new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        btn_show.setText(" error "+statusCode);

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        add.setText(responseString+" status code "+statusCode);
                    }
                });


                break;

        }




    }
}
