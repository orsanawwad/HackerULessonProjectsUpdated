package com.example.adnan.json_example_2;

/**
 * Created by adnan on 10/01/2016.
 */
public class person {

    private  String id;
    private  String name;
    private adress person_adress;





    public  person()
    {

    }

    @Override
    public String toString() {
        return "person{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", person_adress=" + person_adress +
                '}';
    }

    public person(String id, String name, adress person_adress) {
        this.id = id;
        this.name = name;
        this.person_adress = person_adress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public adress getPerson_adress() {
        return person_adress;
    }

    public void setPerson_adress(adress person_adress) {
        this.person_adress = person_adress;
    }

    public   class  adress
    {

        @Override
        public String toString() {
            return "adress{" +
                    "street_name='" + street_name + '\'' +
                    ", street_num=" + street_num +
                    '}';
        }

        private String street_name;
        private int street_num;

        public adress(String street_name, int street_num) {
            this.street_name = street_name;
            this.street_num = street_num;
        }

        public String getStreet_name() {
            return street_name;
        }

        public void setStreet_name(String street_name) {
            this.street_name = street_name;
        }

        public int getStreet_num() {
            return street_num;
        }

        public void setStreet_num(int street_num) {
            this.street_num = street_num;
        }
    }
}
