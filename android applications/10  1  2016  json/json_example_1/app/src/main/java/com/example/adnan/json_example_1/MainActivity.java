package com.example.adnan.json_example_1;

import android.support.annotation.BoolRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    String all_text;
    Button b1,b2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.b1);
        b2 = (Button)findViewById(R.id.b2);
    }


    public void my_click(View v1)
    {
        switch (v1.getId())
        {
            case R.id.b1:
                String res ;
                person per1 = new person(18,"Ahmad ","1234");
                res = convert_from_person_2_json(per1);
                all_text = res;
                b1.setTextSize(24);
                b1.setText(res);
                break;

            case R.id.b2 :
                person res1;
                res1 = convert_from_json_to_person(all_text);
                b2.setTextSize(26);
b2.setText(res1+"");
                break;
        }
    }

    public String convert_from_person_2_json(person p1)
    {
        String all="";


        try {
            JSONObject job1 = new JSONObject();
            job1.put("name",p1.getName());
            job1.put("id",p1.getId());
            job1.put("age",p1.getAge());

            all= job1.toString();
        } catch (JSONException e) {
            Log.i("a1",e.getMessage());
        }


        return all;
    }




    public person convert_from_json_to_person(String json_text)
    {
       person per2=null;
        String name,id;
        int age;
        try {
            JSONObject json2 = new JSONObject(json_text);
            name = json2.getString("name");
            id = json2.getString("id");
            age = json2.getInt("age");
            per2 = new person(age,id,name);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return per2;
    }
}
