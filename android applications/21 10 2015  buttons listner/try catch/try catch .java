package test.adnan.hackeru.calc_with_4_buttons;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int num1,num2,res;
    Button []array = new Button[5];
    // 1 for plus 2 minus 3 mult 4 devide
    int operation;
    EditText ed1,ed2;
    Context cn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
cn1 = this;
        array[0] = (Button)findViewById(R.id.button);
        array[1] = (Button)findViewById(R.id.button2);
        array[2] = (Button)findViewById(R.id.button3);
        array[3] = (Button)findViewById(R.id.button4);
        array[4] = (Button)findViewById(R.id.button5);

        ed1 = (EditText)findViewById(R.id.editText);
        ed2 = (EditText)findViewById(R.id.editText2);
        my_listner m1 = new my_listner();
        for (int i=0;i<array.length;i++)
        {
            array[i].setOnClickListener(m1);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class my_listner implements View.OnClickListener
    {
       @Override
       public void onClick(View my_view)
       {
           String s1,s2;
int num=0;
           try {
num++;
               s1 = ed1.getText().toString();
               num++;
               s2 = ed2.getText().toString();
               num++;
               num1 = Integer.parseInt(s1);
               num++;
               num2 = Integer.parseInt(s2);
               num++;
           }
           catch (Exception ex1)
           {
               Toast.makeText(cn1,ex1.getMessage()+" the num is "+num ,Toast.LENGTH_LONG).show();
           }
           switch ((my_view.getId()))
           {

               case R.id.button:
                  res = num1+num2;
                   break;
               case R.id.button2:
                   res = num1-num2;
                   break;
               case R.id.button3:
                   res = num1*num2;
                   break;
               case R.id.button4:
                   res = num1/num2;
                   break;

           }
           array[4].setText("the result is "+res);

       }
    }

}
