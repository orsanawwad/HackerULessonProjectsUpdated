package test.adnan.hackeru.infrormation_between_activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class add_number extends AppCompatActivity {

    Button b1= null;
    EditText Ed1= null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_number);

        b1 = (Button)findViewById(R.id.button4);
        Ed1 = (EditText)findViewById(R.id.editText);
        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_number, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public class  my_listner implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            Integer num;
            num = Integer.parseInt(Ed1.getText().toString());
            MainActivity.l1.add(num);
        }
    }
}
