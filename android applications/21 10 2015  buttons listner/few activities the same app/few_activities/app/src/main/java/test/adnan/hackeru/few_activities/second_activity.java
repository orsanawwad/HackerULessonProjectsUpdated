package test.adnan.hackeru.few_activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

public class second_activity extends AppCompatActivity {

    Button b1 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int age;
        setContentView(R.layout.activity_second_activity);
        Intent IN = getIntent();
        Bundle BN =  IN.getExtras();
        String my_user = BN.getString("user");
        age = BN.getInt("user_age");
        Toast.makeText(this,my_user,Toast.LENGTH_LONG).show();
        b1 = (Button)findViewById(R.id.button2);
        b1.setText(my_user+age);
        int []a;
        int sum=0;
        a = BN.getIntArray("my_array");
        for (int i=0;i<a.length;i++)
        {
            sum+= a[i];

        }
        b1.setText(b1.getText().toString()+sum);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
