package test.adnan.hackeru.buttons_listner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button b1,b2;

    EditText e1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b1 = (Button)  findViewById(R.id.first);
        b2 = (Button)  findViewById(R.id.b1);
        e1 =(EditText) findViewById(R.id.t1);
        b1.setText("hello i was changed in on create");
        e1.setText("aaa");

        my_listner m1 = new my_listner();
        b1.setOnClickListener(m1);
        b2.setOnClickListener(m1);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public class my_listner implements View.OnClickListener
    {
        @Override
        public void onClick(View v1)
        {
            switch ((v1.getId()))
            {

                case R.id.first:
                    e1.setText("hello u clicked  first button");
                    break;
                case R.id.b1:
                    e1.setText("hello u clicked second button clicked");
                    break;


            }


        }

    }
}
