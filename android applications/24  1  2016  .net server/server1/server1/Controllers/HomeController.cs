﻿using server1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace server1.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }
        [HttpGet]
        public string fun3(int n1, int n2, int n3)
        {
            string res;
            int sum = n1 + n2 + n3;

            res = " sum of thre numbers is " + sum;
            return res;
        }
        [HttpGet]
        public string fun4(int a1 ,int a2 ,int a3)
        {
            string res;

            int sum = a1 * a2 * a3;
            res = "the res is" + sum;
            return res; 
        }


        [HttpPost]
        public string fun2(int n1)
        {
            return "" + (n1 * n1);
        }


        [HttpGet]
        public string fun1(int num)
        {
            int res = 0;
            for (int i = 0; i < num; i++)
            {
                res += i;
            }
            return " result is " + res;
        }

        public string sum()
        {
            int res = 0;
            for (int i = 0; i < 10; i++)
            {
                res += i; 
            }
            return " the result is " + res;
        }


      
        public static int num = 0;
        public string get_num()
        {
            return "num = "+num;
        }
        public void increase_num()
        {
            num++;
        }
        public string get_json(string id)
        {
            person p11 = null; ;
            string res="";
            foreach (person per2 in l1)
                if (per2.Id.Equals(id))
                    p11 = per2;
            var json_obj =new  System.Web.Script.Serialization.JavaScriptSerializer();
            res = json_obj.Serialize(p11);
            return res;
        }
        public string get_car1(int number)
        {
            car car2 = null;
            string res = "";
            foreach(car car2 in l2)
                if (car2.Number.Equals(number))
                    var json_obj =new System.Web.Script.Serialization.JavaScriptSerializer();
            res = Json_obj.serialize(car2);
            return res;
        }
        public string get_name_car(int number)
        {
            string name = "שלום ";
            foreach (car car2 in l2)
                if (car2.Number.Equals(number))
                    number = car2.Number;
            return number;
        }

        public string get_all()
        {
            string res;
           var json_ressult =new   System.Web.Script.Serialization.JavaScriptSerializer();
           res = json_ressult.Serialize(l1);
            
            return res;
        }
       

        public string get_name(string id)
        {
            string name="שלום ";
            foreach(person per2 in l1)
                if(per2.Name.Equals(name))
                    name = per2.Name;
            return name;
        }

        public static List<car> l2 = new List<car>();
        [HttpGet]
        public void add_car(string id, string model, int number)
        {
            car car1 = new car(number, model, id);
            l2.Add(car1);
        }
        public string get_all_cars()
        {
            string all = "";
            foreach (car car1 in l2)
                all += car1.Number;
            return all;
        }
     

        public static List<person> l1 = new List<person>();

        [HttpGet]
        public void add_person(string name, string id)
        {
            person per1 = new person(name, id);
            l1.Add(per1);
        }
        [HttpGet]
        public string get_all_names()
        {
            string all = "";
            foreach (person per11 in l1)
                all += per11.Name;
            return all;
        }
    }
}
