package test.adnan.hackeru.gridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    GridView gr1 =null;
    List<String> my_list=  new ArrayList<String>();
    int flag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            my_list.add("aaa");
            my_list.add("11");

            my_list.add("bb");
            my_list.add("22");

            my_list.add("cc");
            my_list.add("33");

            my_list.add("dd");
            my_list.add("44");
            Toast.makeText(getApplicationContext(),""+flag,Toast.LENGTH_LONG).show();
flag++;
            gr1 = (GridView)findViewById(R.id.gridView);
flag++;
            Toast.makeText(getApplicationContext(),""+flag,Toast.LENGTH_LONG).show();
            ArrayAdapter<String> adpter1 = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1 , my_list);
flag++;


            gr1.setAdapter(adpter1);
            flag++;
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),e.getMessage()+flag,Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
