package test.adnan.hackeru.customize_list2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    int []pics = new int[6];
    TableLayout t1 ;
    List<String>l1 = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        t1 = (TableLayout)findViewById(R.id.t1);

        pics[0] = R.drawable.p1;
        pics[1] = R.drawable.p2;
        pics[2] = R.drawable.p1;
        pics[3] = R.drawable.p4;
        pics[4] = R.drawable.p5;
        int index=0;
        my_listner m1 = new my_listner();

        for (int i=0;i<100;i++)
        {
            TableRow tr1 = new TableRow(this);
            Button b1,b2;
            ImageButton im1;
            b1 = new Button(this);
            b2 = new Button(this);
            im1 = new ImageButton(this);
            im1.setImageResource(pics[index]);
            index++;
            if(index==4)
                index=0;
            b1.setText("phone"+i);
            l1.add(i+"");
            b1.setId(i + 1);
            im1.setId(i + 1001);
            b1.setOnClickListener(m1);
            im1.setOnClickListener(m1);
            tr1.addView(b1);
            tr1.addView(im1);
            t1.addView(tr1);

        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

  class my_listner implements  View.OnClickListener
  {

      @Override
      public void onClick(View v) {
          int index;
          index = v.getId();
          index = index%1000;
          Toast.makeText(getApplicationContext(),l1.get(index), Toast.LENGTH_LONG).show();

      }
  }

}
