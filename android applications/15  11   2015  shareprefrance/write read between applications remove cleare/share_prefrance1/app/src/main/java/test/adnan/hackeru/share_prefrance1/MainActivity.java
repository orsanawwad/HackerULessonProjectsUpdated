package test.adnan.hackeru.share_prefrance1;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button b1,b2,b3,b4,b5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button)findViewById(R.id.button);
        b1.setText("save to share prefrane");

        b2 = (Button)findViewById(R.id.button2);

        b3 = (Button)findViewById(R.id.button3);


        b4 = (Button)findViewById(R.id.button4);

        b5 = (Button)findViewById(R.id.button5);
        b2.setText("get data from  share prefrane");

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sh1 = getApplicationContext().getSharedPreferences("first", MODE_PRIVATE);
                SharedPreferences.Editor ed1 = sh1.edit();
                ed1.putString("str1", "hello i am ur first data ");
                ed1.putInt("num", 100);


                ed1.commit();
            }
        });


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sh2 = getApplicationContext().getSharedPreferences("first",MODE_PRIVATE);
                int number = sh2.getInt("num", -90);
                String str11 = sh2.getString("str1",null);
                b2.setText(str11 + " num = " + number);

            }
        });



        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sh3 = getApplicationContext().getSharedPreferences("second", MODE_WORLD_READABLE);
                SharedPreferences.Editor ed3 = sh3.edit();
                ed3.putString("str1","hello from another application");
                ed3.commit();
            }
        });

        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sh1 = getApplicationContext().getSharedPreferences("first", MODE_PRIVATE);
                SharedPreferences.Editor ed1 = sh1.edit();
                ed1.remove("num");
                ed1.commit();
            }
        });

        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sh1 = getApplicationContext().getSharedPreferences("first", MODE_PRIVATE);
                SharedPreferences.Editor ed1 = sh1.edit();
                ed1.clear();
                ed1.commit();
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
