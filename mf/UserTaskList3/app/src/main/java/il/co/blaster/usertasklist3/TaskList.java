package il.co.blaster.usertasklist3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;

public class TaskList extends AppCompatActivity {

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        context = this;
        Globals.getInstance().setCurrentContext(context);
        PrefMan.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tasklist, menu);
        return true;
    }

    public void TLogout(View view){
        PrefMan.getInstance().logOut();
        Intent goToLogin = new Intent(context, Login.class);
        context.startActivity(goToLogin);
    }
}
