package il.co.blaster.usertasklist3;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;


//init singleton

public class PrefMan {
	private static PrefMan ourInstance = new PrefMan();

	public static PrefMan getInstance() {
		return ourInstance;
	}

	private PrefMan() {
	}

	//declarations

	private static Context globalContext = Globals.getCurrentContext();
	private static SharedPreferences UserRegistry = globalContext.getSharedPreferences("UserRegistry", Context.MODE_PRIVATE);
	private static SharedPreferences LoggedInRegistry = globalContext.getSharedPreferences("LoggedInRegistry", Context.MODE_PRIVATE);

/* section: authentication */

	//check user logged in

	public static boolean checkLoggedIn() {
		debugMe("checkLoggedIn");
		if (!LoggedInRegistry.contains("LoggedInUser")) {
			LoggedInRegistry.edit().putString("LoggedInUser", "").commit();
			System.out.println("***************** key not found");
			return false;
		} else {
			String loggedInUser = LoggedInRegistry.getString("LoggedInUser", "");
			System.out.println(loggedInUser);
			if (loggedInUser.equals("")) {
				System.out.println("***************** empty val");
				return false;
			} else if (UserRegistry.contains(loggedInUser)) {
				System.out.println("***************** match in userreg");
				debugMe("checkLoggedIn true");
				return true;
			} else {
				System.out.println("***************** wtf just happened");
				return false;
			}
		}
	}

	//validate user from login
	public static boolean validateUser(String _user, String _pass) {
		if (_user.equals("") || _pass.equals("")) {
			Toast.makeText(globalContext, "Please enter username and password", Toast.LENGTH_LONG).show();
			return false;
		} else if (!UserRegistry.contains(_user)) {
			Toast.makeText(globalContext, "User doesn't exist", Toast.LENGTH_LONG).show();
			return false;
		} else if (!_pass.equals(UserRegistry.getString(_user, "ErrorStr"))) {
			Toast.makeText(globalContext, "Password doesn't match user", Toast.LENGTH_LONG).show();
			return false;
		} else if (_pass.equals(UserRegistry.getString(_user, "ErrorStr"))) {
			LoggedInRegistry.edit().putString("LoggedInUser", _user).commit();
			Toast.makeText(globalContext, "User " + _user + " logged in", Toast.LENGTH_LONG).show();
			System.out.println();
			debugMe("validateUser");
			return true;
		} else
			return false;
	}

	//add new user

	public static boolean addUser(String _user, String _pass, String _pass2) {
		if (_user.equals("") || _pass.equals("") || _pass2.equals("")) {
			Toast.makeText(globalContext, "Please enter username and password", Toast.LENGTH_LONG).show();
			return false;
		} else if (UserRegistry.contains(_user)) {
			Toast.makeText(globalContext, "User exists!", Toast.LENGTH_LONG).show();
			return false;
		} else if (!_pass.equals(_pass2)) {
			Toast.makeText(globalContext, "Passwords do not match", Toast.LENGTH_SHORT).show();
			return false;
		} else try {
			UserRegistry.edit().putString(_user, _pass).commit();
			debugMe("addUser");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(globalContext, "Something went wrong! Could not add user", Toast.LENGTH_SHORT).show();
			return false;
		}
	}

	//get logged in user
	public static String getLoggedInUser() {
		return checkLoggedIn() ? LoggedInRegistry.getString("LoggedInUser", "") : "ErrorUser!!!";
	}

	//logout

	public static void logOut() {
		LoggedInRegistry.edit().putString("LoggedInUser", "");
	}

	//delete user
	public static boolean deleteUser() {
		String _user = LoggedInRegistry.getString("LoggedInUser", "");
		try {
			UserRegistry.edit().remove(_user).commit();
			LoggedInRegistry.edit().putString("LoggedInUser", "");
			// +delete tasklist
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(globalContext, "Something went wrong! Could not delete user", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	//change password

	private static void debugMe(String myFunc) {
		if (Globals.MYDBGMODE) {
			System.out.println("********************************************");
			System.out.println("invoked from " + myFunc);
			try {
				System.out.println(UserRegistry.getAll().toString());
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error getting UserRegistry");
			}
			try {
				System.out.println(LoggedInRegistry.getAll().toString());
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error getting LoggedInRegistry");
			}
			System.out.println("********************************************");
		}
	}

/* section: tasklist */

	public static void getUserTasks(String _user){
		_user = getLoggedInUser().toString();
	}

} //EOF






