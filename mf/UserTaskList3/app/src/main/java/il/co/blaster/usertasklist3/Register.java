package il.co.blaster.usertasklist3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class Register extends AppCompatActivity {

	EditText RUser, RPass, RPass2;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		EditText RUser;
		EditText RPass;
		EditText RPass2;

		context = this;
		Globals.getInstance().setCurrentContext(context);

	}

	@Override
	protected void onResume() {
		super.onResume();
		context = this;
		Globals.getInstance().setCurrentContext(context);
		PrefMan.getInstance();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_register, menu);
		return true;
	}

	public void RRegister(View view) {
		RUser = (EditText) findViewById(R.id.RNewUser);
		RPass = (EditText) findViewById(R.id.RNewPass);
		RPass2 = (EditText) findViewById(R.id.RNewPass2);
		String RUserTxt = RUser.getText().toString();
		String RPassTxt = RPass.getText().toString();
		String RPass2Txt = RPass2.getText().toString();
		if (PrefMan.addUser(RUserTxt, RPassTxt, RPass2Txt)) {
			AlertDialog.Builder chooseNextStep = new AlertDialog.Builder(context);
			chooseNextStep
					.setTitle("Success!")
					.setMessage("User " + RUser.getText().toString() + " registered successfully.\nWhat would you like to do now?")
					.setPositiveButton("Login new user", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//login
						}
					})
					.setNegativeButton("\nGo back to login screen", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent goToMain = new Intent(context, Login.class);
							context.startActivity(goToMain);
							finish();
						}
					})
					.setNeutralButton("\nRegister new user", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent addUser = new Intent(context, Register.class);
							context.startActivity(addUser);
							finish();
						}
					})
					.show();
		}
	}

	public void RCancel(View view) {
		Intent goToMain = new Intent(context, Login.class);
		context.startActivity(goToMain);
		finish();
	}

}
