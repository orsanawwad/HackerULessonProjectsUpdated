package il.co.blaster.usertasklist3;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.List;
import java.util.Map;

/**
 * Created by ikarm on 3/27/16.
 */
public class TaskMan {

	//private declarations

	private Context globalContext = Globals.getCurrentContext();
	private String currentUser = PrefMan.getLoggedInUser();
	private SharedPreferences userTasksRegistry;
	private List<TaskMan> userTasks;

	//available properties

	public String singleTask;
	public boolean taskState;

	//constructor for adding an instance with task

	public TaskMan(String _singleTask, boolean _taskState){
		this.singleTask = _singleTask;
		this.taskState = _taskState;
	}

	public TaskMan(){
		userTasksRegistry = globalContext.getSharedPreferences(currentUser, Context.MODE_PRIVATE);
		//userTasks = userTasksRegistry.getAll();
	}

	public void populateTasks(){
		Map<String, ?> allTasks = userTasksRegistry.getAll();
		for (Map.Entry<String, ?> singleTask:allTasks.entrySet()){
			userTasks.add(new TaskMan(singleTask.getKey())
		}
	}
}
