package poly_morphisim2;

public class egol extends square{

	public double shetah()
	{
		return this.len*this.len*Math.PI;
	}
	
	public double hekef()
	{
		return this.len*Math.PI*2;
	}

	public egol(int len) {
		super(len);
		// TODO Auto-generated constructor stub
	}

	public String toString() {
		return "egol []";
	}
	
	
}
