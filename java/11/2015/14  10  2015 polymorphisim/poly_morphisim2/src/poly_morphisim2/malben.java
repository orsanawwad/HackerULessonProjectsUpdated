package poly_morphisim2;

public class malben extends square{

	protected int withd;
	public double shetah()
	{
		return this.withd*this.len;
	}
	public int getWithd() {
		return withd;
	}
	public void setWithd(int withd) {
		this.withd = withd;
	}
	public malben(int len, int withd) {
		super(len);
		this.withd = withd;
	}
	public double hekef()
	{
		return 2*(this.withd+this.len);
	}
	public String toString() {
		return "malben [withd=" + withd + "]";
	}
	
	
}
