package classes_3;

public class product {
	
	private int pro_code;
	private String name;
	private int quan_in;
	private int quan_min;
	private int price;
	public  static int counter=0;
	public product()
	{
		counter++;
		this.pro_code= counter;
	}
	public product(String name, int quan_in, int quan_min, int price) {
		super();
		counter++;
		this.pro_code = counter;
		this.name = name;
		this.quan_in = quan_in;
		this.quan_min = quan_min;
		this.price = price;
	}
	public int getPro_code() {
		return pro_code;
	}
	public void setPro_code(int pro_code) {
		this.pro_code = pro_code;
	}
	
	public int sale(int quantity)
	{
		if(this.quan_in < quantity)
		{
			System.out.println(Messages.getString("product.0")+this.quan_in); //$NON-NLS-1$
			return -1;
			
		}
		quan_in-= quantity;
		return (this.price*quantity);
	}
	public void change_price(int new_price)
	{
		if(new_price<=0)
		{
			System.out.println(Messages.getString("product.1")); //$NON-NLS-1$
			return;
		}
		this.price = new_price;
	}
	public String toString() {
		return "product [pro_code=" + pro_code + ", name=" + name
				+ ", quan_in=" + quan_in + ", quan_min=" + quan_min
				+ ", price=" + price + "]";
	}

}
