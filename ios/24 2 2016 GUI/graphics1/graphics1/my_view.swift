//
//  my_view.swift
//  graphics1
//
//  Created by HackerU on 24/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

   
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 2.0)
        var my_color = UIColor.redColor().CGColor
        CGContextSetStrokeColorWithColor(context, my_color)
        CGContextMoveToPoint(context, 100, 100)
        CGContextAddLineToPoint(context, 200, 300)
        CGContextAddLineToPoint(context, 250, 180)
        var r1 = CGRectMake(200, 200, 100, 150)
        var r2 = CGRectMake(100, 200, 100, 180)
        CGContextAddEllipseInRect(context, r2)
        CGContextAddRect(context, r1)
        CGContextStrokePath(context)
    }
   

}
