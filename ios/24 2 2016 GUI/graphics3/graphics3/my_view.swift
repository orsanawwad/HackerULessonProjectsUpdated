//
//  my_view.swift
//  graphics3
//
//  Created by HackerU on 24/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

    
    var array = [CGPoint]()
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let context  = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(context, 4.0)
        var color = UIColor.redColor().CGColor
        CGContextSetStrokeColorWithColor(context, color)
        for e1 in array
        {
             var r1 = CGRectMake(e1.x, e1.y, 50, 30)
         CGContextAddEllipseInRect(context, r1)
        }
        CGContextStrokePath(context)
        
        
    }
   
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
let  touch = (touches.first! as UITouch)
        
        var my_location = touch.locationInView(self)
        array.append(my_location)
        setNeedsDisplay()
    }

}
