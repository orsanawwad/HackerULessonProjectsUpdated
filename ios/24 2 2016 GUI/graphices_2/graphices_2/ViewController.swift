//
//  ViewController.swift
//  graphices_2
//
//  Created by HackerU on 24/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var v1: my_view!
    @IBAction func btn_add_rect(sender: AnyObject) {
        v1.draw(2)
    }

    @IBAction func btn_add_line(sender: AnyObject) {
        v1.draw(1)
    }
}

