//
//  ViewController.swift
//  test1
//
//  Created by HackerU on 23/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController , UITableViewDataSource {
var names = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        add_to_names()
        table1.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func f1()->String
    {
        let name = "abcd"
        var age = 20
        var str : String
        
        str = "{\"name\":\""+name+"\",\"age\":\""+"\(age)"+"\"}"
        
        return str
    }
    
    @IBAction func show_all_school_as_json(sender: AnyObject) {
        
        var s1 = school()
        var p1 = person(name: "rajih", age: 25)
        s1.add_student(p1)
        var p2 = person(name: "hosni", age: 23)
        s1.add_student(p2)
        var p3 = person(name: "ioeal  ", age: 28)
        s1.add_student(p3)
        var all = s1.convert_to_json()
        NSLog("%@", all)
    
        
    }
    @IBAction func get_name_from_person(sender: AnyObject) {
        var per1 = person(name: "sonia", age: 22)
        
        var str = per1.convert_to_json()
        var my_data = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        var my_json = JSON(data:my_data!,options:NSJSONReadingOptions.MutableContainers,error:nil)
var name = my_json["person_name"]
        NSLog("%@", "\(name)")
        
        
    }
    
    @IBAction func show_person_as_json(sender: AnyObject) {
        
        var per1 = person(name: "ahmad", age: 19)
        NSLog("%@", "\(per1.convert_to_json())")
    }
    @IBAction func read_json(sender: AnyObject) {
        
        var res = f1()
        NSLog("%@", res)
        
        var my_data = res.dataUsingEncoding(NSUTF8StringEncoding)
        
        var my_json = JSON(data:my_data!,options:NSJSONReadingOptions.MutableContainers,error:nil)
        
        var name1 = my_json["name"]
        NSLog("%@", "\(name1)")
        
    }

    @IBOutlet weak var table1: UITableView!
    @IBAction func show_all_array_as_json(sender: AnyObject) {
        
        read_array_of_json()
    }
    func read_array_of_json()
    {
        
        
        var s1 = school()
        var p1 = person(name: "rajih", age: 25)
        s1.add_student(p1)
        var p2 = person(name: "hosni", age: 23)
        s1.add_student(p2)
        var p3 = person(name: "ioeal  ", age: 28)
        s1.add_student(p3)
        var str  = s1.convert_to_json()
        
        var my_data = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        var my_json = JSON(data:my_data!,options:NSJSONReadingOptions.MutableContainers,error:nil)
        NSLog("%@", "\(my_json.count)")
        
        
        
        var index : Int
        for index = 0 ; index  < my_json.count ; index++
        {
            var name = my_json[index]["person_name"]
            var age = my_json[index]["age"]
            NSLog("%@", "\(name)")
            NSLog("%@", "\(age)")
            
        }
        
        
        
        
    }
    func add_to_names()
    {
        var s1 = school()
        var p1 = person(name: "rajih", age: 25)
        s1.add_student(p1)
        var p2 = person(name: "hosni", age: 23)
        s1.add_student(p2)
        var p3 = person(name: "ioeal  ", age: 28)
        s1.add_student(p3)
        var str  = s1.convert_to_json()
        
        var my_data = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        var my_json = JSON(data:my_data!,options:NSJSONReadingOptions.MutableContainers,error:nil)
        NSLog("%@", "\(my_json.count)")
        
        
        
        var index : Int
        for index = 0 ; index  < my_json.count ; index++
        {
            var name = my_json[index]["person_name"]
            var age = my_json[index]["age"]
           names.append("\(name)")
        }

    }
    
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var my_cell  = UITableViewCell()
        
        my_cell.textLabel?.text = names[indexPath.row]
        
        
        return my_cell
    }
    
    
    
    
    
    
    


}

