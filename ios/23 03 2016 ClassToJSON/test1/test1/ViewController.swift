//
//  ViewController.swift
//  test1
//
//  Created by HackerU on 23/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func f1()->String
    {
        var name = "abcd"
        var age = 20
        var str : String
        
        str = "{\"name\":\""+name+"\",\"age\":\""+"\(age)"+"\"}"
        
        return str
    }
    
    @IBAction func get_name_from_person(sender: AnyObject) {
        var per1 = person(name: "sonia", age: 22)
        
        var str = per1.convert_to_json()
        var my_data = str.dataUsingEncoding(NSUTF8StringEncoding)
        
        var my_json = JSON(data:my_data!,options:NSJSONReadingOptions.MutableContainers,error:nil)
var name = my_json["person_name"]
        NSLog("%@", "\(name)")
        
        
    }
    
    @IBAction func show_person_as_json(sender: AnyObject) {
        
        var per1 = person(name: "ahmad", age: 19)
        NSLog("%@", "\(per1.convert_to_json())")
    }
    @IBAction func read_json(sender: AnyObject) {
        
        var res = f1()
        NSLog("%@", res)
        
        var my_data = res.dataUsingEncoding(NSUTF8StringEncoding)
        
        var my_json = JSON(data:my_data!,options:NSJSONReadingOptions.MutableContainers,error:nil)
        
        var name1 = my_json["name"]
        NSLog("%@", "\(name1)")
        
    }



}

