//
//  main.swift
//  deinit
//
//  Created by HackerU on 14/02/2016.
//  Copyright © 2016 HackerU. All rights reserved.
//

import Foundation

print("Hello, World!")





class class1
{
    
    var num : Int16
    var name : String?
    init(num : Int16)
    {
        self.num = num
    }
    deinit
    {
        print("destrustor was calling")
    }
}

func f1()
{
    var c1 : class1
    c1 = class1(num: 10)
}

f1()

var c2: class1?
c2 = class1(num: 20)
c2 = nil
var c3 : class1
c3 = class1(num: 40)
// non optional var cant be nil
//c3 = nil


class class2 {
    var x :Int16    = 10
    func f1(num : Int16)
    {
        self.x += num
    }
    
}

struct stu1 {
    var x: Int16
    mutating  func f1(num : Int)
    {
        self.x   += num
    }
}











