//
//  ViewController.swift
//  mmm
//
//  Created by HackerU on 02/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UITableViewDataSource {

    var names = ["sonia","ahmad","yaron","ioeal"]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var table1: UITableView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var t1: UITextField!
    @IBAction func add(sender: AnyObject) {
        
        var s1 : String
        s1 = t1.text!
        names.append(s1)
        table1.reloadData()
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        var cell :UITableViewCell
        cell = UITableViewCell()
        cell.textLabel?.text = names[indexPath.row]
        return cell
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return names.count
    }

}

