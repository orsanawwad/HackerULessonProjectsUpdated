//
//  ViewController.swift
//  coreData3_6_4_2016
//
//  Created by HackerU on 06/04/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    //    add_person("aa", adress: "rahaov ww haifa", age: 20)
          //  add_person("aa", adress: "rahaov ww haifa kkk", age: 34)
          //  add_person("bb", adress: "haifa ww  mmm", age: 52)
           // add_person("ccc", adress: "rahaov tel avev ", age: 19)
           // add_person("dd", adress: "rahaov akko nnn", age: 70)
           // add_person("aa", adress: "rahaov ww haifa", age: 23)
     //   get_all_data()
        get_by_filtering()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func add_person(name: String,adress: String,age :NSNumber)
    {
        
        let manageobjectcontxt = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: manageobjectcontxt)
        let person = Entity(entity:entityDescription!,insertIntoManagedObjectContext:manageobjectcontxt)
        
        person.name = name
        person.adress = adress
        person.age = age
        
        do{
            
            try   manageobjectcontxt.save()
        }
        catch
        {
            
        }
    }
    
    func get_all_data()
    {
        
        let manageobjectcontxt = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: manageobjectcontxt)
        
        let request = NSFetchRequest()
        request.entity = entityDescription
        
        do
        {
            let results = try manageobjectcontxt.executeFetchRequest(request)
            var ii : Int
            for ii = 0 ; ii < results.count ; ii++
            {
                
                let match = results[ii]  as! NSManagedObject
                
                let age = match.valueForKey("age") as! NSNumber
                let name = match.valueForKey("name") as! String
                let adress = match.valueForKey("adress") as! String
                NSLog("%@", "\(age)"+"  "+"\(name)"+"  "+"\(adress)")
            }
            
        }
        catch
        {
            
        }
    }
    
    func get_by_filtering()
    {
        let manageobjectcontxt = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: manageobjectcontxt)
        
        let request = NSFetchRequest()
        request.entity = entityDescription
        
        let pred1 = NSPredicate(format: "%K > %D and %K < %D","age",30,"age",60)
        let par_name = "adress"
        let val = "*haifa"
        let pred2 = NSPredicate(format: "%K like %@ ",par_name,val)
        
        request.predicate    = pred2
        
        do
        {
            let results = try manageobjectcontxt.executeFetchRequest(request)
            var ii : Int
            for ii = 0 ; ii < results.count ; ii++
            {
                
                let match = results[ii]  as! NSManagedObject
                
                let age = match.valueForKey("age") as! NSNumber
                let name = match.valueForKey("name") as! String
                let adress = match.valueForKey("adress") as! String
                NSLog("%@", "\(age)"+"  "+"\(name)"+"  "+"\(adress)")
            }
            
        }
        catch
        {
            
        }

    }
    


}


