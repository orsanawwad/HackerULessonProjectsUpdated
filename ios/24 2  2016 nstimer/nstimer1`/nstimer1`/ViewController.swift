//
//  ViewController.swift
//  nstimer1`
//
//  Created by HackerU on 24/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    var my_timer = NSTimer()
    var num : Int16 = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func start(sender: AnyObject) {
        

        my_timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "counter", userInfo: nil, repeats: true)
        
        
    }

    @IBAction func speed(sender: AnyObject) {
        
        my_timer.invalidate()
        my_timer = NSTimer.scheduledTimerWithTimeInterval(0.25, target: self, selector: "counter", userInfo: nil, repeats: true)
        
    }
    func counter()
    {
        num++
        var s1 :String?
        s1 = ("\(num)")
        label1.text = s1
    }
    
    @IBOutlet weak var label1: UILabel!
}

