//
//  my_view.swift
//  dynamic-design4
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

    var label1 : UILabel?

    var btn1 : UIButton?
    
    override  init(frame: CGRect) {
        
        super.init(frame: frame)
        
        var r1 = CGRectMake(0, 0, 50, 50)
        label1 = UILabel(frame: r1)
        label1!.text = "first label"
        label1?.backgroundColor = UIColor.yellowColor()
        self.addSubview(label1!)
        var r2 = CGRectMake(60, 60, 50, 50)
        btn1 = UIButton(frame: r2)
        self.addSubview(btn1!)
        btn1?.setTitle("btn dynamic", forState: .Normal)
        self.backgroundColor = UIColor.greenColor()
        self.addSubview(btn1!)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
