//
//  ViewController.swift
//  dynamic_design3
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var v1 : UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func add_view(sender: AnyObject) {
        
        var btn1 : UIButton
        var r2 = CGRectMake(0, 0, 50, 50)
        btn1 = UIButton(frame: r2)
        btn1.backgroundColor = UIColor.yellowColor()
        btn1.setTitle("my click", forState: .Normal)
        var r1 = CGRectMake(50, 50, 200, 400)
        btn1.tintColor = UIColor.blackColor()
        
        v1 = UIView(frame: r1)
        
        v1?.backgroundColor = UIColor.greenColor()
        v1?.addSubview(btn1)
        self.view.addSubview(v1!)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

