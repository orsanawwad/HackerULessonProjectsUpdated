//
//  ViewController.swift
//  dynamic_button
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var btn2 : UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        btn1.addTarget(self, action: "f1:", forControlEvents: .TouchUpInside)
        var r1 = CGRectMake(100, 100, 100, 100)
        
        btn2 = UIButton(frame: r1)
        btn2?.setTitle("i am second button", forState: .Normal)
        btn2?.backgroundColor = UIColor.redColor()
        btn2?.addTarget(self, action: "f1:", forControlEvents: .TouchUpInside)
        self.view.addSubview(btn2!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBOutlet weak var lable1: UILabel!
    @IBOutlet weak var btn1: UIButton!
    
    @IBAction func f1(sender : UIButton!)
    {
        lable1.text = "hello from dynamic action"
    }
}

