//
//  ViewController.swift
//  dynamic5
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var v1 : my_view?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        var r1  = CGRectMake(50, 50, self.view.bounds.width , self.view.bounds.height)
        v1 = my_view(frame: r1)
        self.view.addSubview(v1!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

