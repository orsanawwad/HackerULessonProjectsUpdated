//
//  ViewController.swift
//  dynamic_button2
//
//  Created by HackerU on 09/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var label1 : UILabel?
    var buttons = [UIButton]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var index : Int
        for index = 0 ; index < 5 ; index++
        {
            var b1 : UIButton
            var r1 = CGRectMake(50, CGFloat (index * 50) , 40, 30)
            b1 = UIButton(frame: r1)
            b1.setTitle("\(index)", forState: .Normal)
            b1.backgroundColor = UIColor.greenColor()
            b1.tag = index
            b1.addTarget(self, action: "btn_click:", forControlEvents: .TouchUpInside)
            self.view.addSubview(b1)
            buttons.append(b1)
            
        }
        var r2 = CGRectMake(50, CGFloat((index+1)*50), 100, 50)
         label1 = UILabel(frame: r2)
        label1!.textColor = UIColor.blackColor()
        label1!.backgroundColor = UIColor.yellowColor()
        self.view.addSubview(label1!)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btn_click( sender : UIButton!)
    {
        label1?.text = "\(sender.tag)"
        
    }


}

