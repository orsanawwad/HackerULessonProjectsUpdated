package com19.example.hackeru.firebase_boker1;

/**
 * Created by hackeru on 4/13/2016.
 */
public class person {
    private String name;

    public person()
    {

    }

    public person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
