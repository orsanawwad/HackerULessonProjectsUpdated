package com19.example.hackeru.firebase3_boker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

public class MainActivity extends AppCompatActivity  implements android.view.View.OnClickListener{

    Button add_fr,add_fm,add_wo,get_all_friendes;
    EditText ed_name,ed_adress,ed_age;
    Firebase fbsFr,fbsFam,fbsWork ;
    Firebase fbsall;
    Button add_by_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        fbsall =  new Firebase("https://iostest11.firebaseio.com/");

        fbsWork=  fbsall.child("work");
        fbsFam = fbsall.child("family");
        fbsFr = fbsall.child("friendes");

        add_fm = (Button)findViewById(R.id.add_fam);
        add_fr = (Button)findViewById(R.id.add_fr);
        add_wo = (Button)findViewById(R.id.add_work);
        add_by_name = (Button)findViewById(R.id.button);
        add_by_name.setOnClickListener(this);
get_all_friendes = (Button)findViewById(R.id.get_all_friends) ;
        ed_adress = (EditText)findViewById(R.id.ed_adress);
        ed_name = (EditText)findViewById(R.id.ed_name);
        ed_age = (EditText)findViewById(R.id.ed_age);
        add_fm.setOnClickListener(this);
        add_fr.setOnClickListener(this);
        add_wo.setOnClickListener(this);
        get_all_friendes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        contacts cn1;
        cn1 = add_person();
        switch (v.getId())
        {
            case R.id.add_fam:
                fbsFam.push().setValue(cn1);
                break;
            case R.id.add_fr:
                fbsFr.push().setValue(cn1);
                break;

            case R.id.add_work:
                fbsWork.push().setValue(cn1);
                break;
            case R.id.get_all_friends:


                try {
                    Firebase friendsFbs = fbsall.child("friendes");
                    friendsFbs.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            for (DataSnapshot ds:dataSnapshot.getChildren())
                            {
                               contacts cn11 = ds.getValue(contacts.class);
                               add_fr.setText(add_fr.getText().toString()+cn11);
                            }
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                } catch (Exception e) {
                   Log.i("a2",e.getMessage());
                }


                break;

            case R.id.button:
                try {
                    String name;
                  name=   ed_name.getText().toString();
                    Log.i("a3","name =   ."+name+".");
                    Firebase fbsd = fbsall.child(name.trim());

                    fbsd.push().setValue(cn1);
                } catch (Exception e) {
                   Log.i("a3",e.getMessage());
                }
        }


    }

    public contacts add_person()
    {
        String name,adress;
        int age;
        name = ed_name.getText().toString();
        adress = ed_adress.getText().toString();
        age = Integer.parseInt(ed_age.getText().toString());
        contacts cn1 = new contacts(adress,name,age);

        return  cn1;

    }
}
