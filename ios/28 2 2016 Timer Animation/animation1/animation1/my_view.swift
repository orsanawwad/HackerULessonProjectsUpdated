//
//  my_view.swift
//  animation1
//
//  Created by HackerU on 28/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class my_view: UIView {

   
    var my_timer : NSTimer?
    var x1 : CGFloat = 50
    var y1 : CGFloat = 50
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let context = UIGraphicsGetCurrentContext()
        let color = UIColor.greenColor().CGColor
        var r1  = CGRectMake(x1, y1, 50, 40 )
         CGContextSetStrokeColorWithColor(context, color)
        CGContextFillEllipseInRect(context,r1)
    
       
      //  CGContextFillPath(context)
       CGContextStrokePath(context)
    }
    
    func f1( flag :Int    )
    {
        
        if flag == 1
        {
        self.bounds.width
        
        my_timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "move_left", userInfo: nil, repeats: true)
        }
        else
            if flag == 2
            {
                my_timer?.invalidate()
                my_timer = nil
                my_timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "move_r", userInfo: nil, repeats: true)
        }
        else
                if flag == 3
                {
                    my_timer?.invalidate()
                    my_timer = nil
                    my_timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "move_d", userInfo: nil, repeats: true)
        }
        else
                    if flag == 4
                    {
                        my_timer?.invalidate()
                        my_timer = nil
                        my_timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "move_up", userInfo: nil, repeats: true)
        }
        
    }
    
    func move_left()
    {
        x1 -= 5
        setNeedsDisplay()
    }
    func move_r()
    {
        x1 += 5
        setNeedsDisplay()
    }
    func move_d()
    {
        y1 += 5
        setNeedsDisplay()
    }
    func move_up()
    {
        y1 -= 5
        setNeedsDisplay()
    }
   

}
