//
//  ViewController.swift
//  timer11
//
//  Created by HackerU on 28/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var label1: UILabel!
    var my_timer : NSTimer?
    var t1 : Double  = 1.0
    var num : Int16=0
    @IBAction func timer_action(sender: AnyObject) {
        
        var b1 : UIButton
        b1 = (sender as? UIButton)!
        if b1.tag == 1
        {
            my_timer = NSTimer.scheduledTimerWithTimeInterval(t1, target: self, selector: "f1", userInfo: nil, repeats: true)
            
        }
        else
        if b1.tag == 2
        {
            my_timer?.invalidate()
            my_timer = nil
        }
        else
        {
            t1 /= 2.0
             my_timer = NSTimer.scheduledTimerWithTimeInterval(t1, target: self, selector: "f1", userInfo: nil, repeats: true)
            
        }
        
    }
    func f1()
    {
        num++;
        var s1 : String
        s1 = ("\(num)")
        label1.text = s1
    }
}

