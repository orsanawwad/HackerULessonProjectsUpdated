//
//  ViewController.swift
//  multy_views
//
//  Created by HackerU on 24/02/2016.
//  Copyright © 2016 HackerU. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var a1 = [Int16]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var label1: UILabel!

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        var v1 : my_view
        v1 = (segue.destinationViewController   as! my_view)
        v1.a1 = a1
    }
    
    @IBAction func btn_sum(sender: AnyObject) {
        
        var sum : Int16 = 0
        for num in a1
        {
            sum  +=  num
        }
        var s1 : String?
        s1 = ("\(sum)")
label1.text = s1
    }
}

