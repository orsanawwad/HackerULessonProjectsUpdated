//
//  ViewController.swift
//  httpost1
//
//  Created by HackerU on 04/05/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit
import SwiftHTTP
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        my_function()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func my_function()
    {
        do
        {
            let opt = try HTTP.GET("http://krowges-001-site1.etempurl.com/")
            opt.start{ response in
            
                if let err =  response.error
                {
                    
                    return
                    
                }
                
                NSLog("%@", "\(response.text)")
            
            }
            
        }
        catch
        {
            
        }
    }


}

