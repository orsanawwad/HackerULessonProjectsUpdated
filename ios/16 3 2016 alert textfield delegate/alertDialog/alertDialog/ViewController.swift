//
//  ViewController.swift
//  alertDialog
//
//  Created by HackerU on 16/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label1: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    var text1 : UITextField?
    @IBAction func btn_action(sender: AnyObject) {
        
        var alert1 = UIAlertController(title: "hello", message: "my mssege", preferredStyle: .Alert)
        
        var my_action = UIAlertAction(title: "take value from text", style: .Default)
            {
                (action:UIAlertAction!) in
                self.label1.text = self.text1?.text
                
        }
        alert1.addAction(my_action)
        
        alert1.addTextFieldWithConfigurationHandler{(textField) -> Void in
        
            self.text1 =  textField
        
        }
        
        self.presentViewController(alert1, animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

