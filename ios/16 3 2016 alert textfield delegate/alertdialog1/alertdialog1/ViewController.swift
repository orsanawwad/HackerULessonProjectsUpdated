//
//  ViewController.swift
//  alertdialog1
//
//  Created by HackerU on 16/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var label1: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func show_alert(sender: AnyObject) {
        
        var my_alert : UIAlertController
        my_alert = UIAlertController(title: "alert message", message: "alert title", preferredStyle: .Alert)
        
        var first_action : UIAlertAction
        first_action = UIAlertAction(title: "ok", style: .Cancel)
            {
                (action:UIAlertAction!) in
            self.label1.text    = "ok"
        }
        my_alert.addAction(first_action)
        
        var second_action : UIAlertAction
        second_action = UIAlertAction(title: "no tnx", style: .Default)
            {
                (action:UIAlertAction!) in
                self.label1.text    = "no tnx"
        }
    var    second_action1 = UIAlertAction(title: "naaaa", style: .Default)
            {
                (action:UIAlertAction!) in
                 self.label1.text    = "naaaa"
        }
        my_alert.addAction(second_action1)
        my_alert.addAction(second_action)
        self.presentViewController(my_alert, animated: true, completion: nil)
    }

}

