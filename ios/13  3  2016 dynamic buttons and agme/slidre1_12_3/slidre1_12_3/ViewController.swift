//
//  ViewController.swift
//  slidre1_12_3
//
//  Created by HackerU on 13/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func slider_value_changed(sender: UISlider) {
        
        label1.text = "\(sender.value)"
     
    }
    @IBOutlet weak var label1: UILabel!

    @IBAction func slider_2_change(sender: UISlider) {
        
        
        var color : UIColor
        color = UIColor(red: CGFloat (sender.value), green: CGFloat (sender.value), blue: CGFloat (sender.value), alpha: CGFloat (sender.value))
        
             label1.text = "\(sender.value)"
        label1.backgroundColor = color  
    }
}

