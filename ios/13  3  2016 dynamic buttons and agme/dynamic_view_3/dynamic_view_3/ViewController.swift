//
//  ViewController.swift
//  dynamic_view_3
//
//  Created by HackerU on 13/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var mv1 : my_view?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        var r11 = CGRectMake(50, 50, 300, 300)
        mv1 = my_view(frame: r11)
        self.view.addSubview(mv1!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
       
        
    }


}

