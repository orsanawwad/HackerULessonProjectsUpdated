//
//  ViewController.swift
//  dynamic_view
//
//  Created by HackerU on 13/03/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var slider1 : UISlider?
    var v1 : UIView?
    var b1: UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        var r1 = CGRectMake(40, 40, 200, 200)
        v1 = UIView(frame: r1)
        v1?.backgroundColor = UIColor.yellowColor()
        self.view.addSubview(v1!)
        var r2 = CGRectMake(0, 0, 100   , 40)
        b1 = UIButton(frame: r2)
        v1?.addSubview(b1!)
        b1?.setTitle("button1", forState: .Normal)
        b1?.backgroundColor = UIColor.greenColor()
        
        var r3 = CGRectMake(60, 60, 50, 50)
        slider1 = UISlider(frame: r3)
        v1?.addSubview(slider1!)
        slider1?.maximumValue = 200
        slider1?.addTarget(self, action: "slider_change:", forControlEvents: .ValueChanged)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func slider_change(sender : UISlider!)
    {
        b1?.setTitle("\(slider1?.value)", forState: .Normal)
    }


}

