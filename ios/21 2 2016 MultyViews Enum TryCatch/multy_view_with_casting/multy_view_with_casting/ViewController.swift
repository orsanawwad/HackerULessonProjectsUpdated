//
//  ViewController.swift
//  multy_view_with_casting
//
//  Created by HackerU on 21/02/2016.
//  Copyright © 2016 adnannet. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let v2 = (segue.destinationViewController as? my_second)
        {
           v2.st2 = "aaa";
        }
        else
            if let v3 = (segue.destinationViewController as? thered)
        {
            
            v3.st3 = "bbbb"
        }
    }


}

