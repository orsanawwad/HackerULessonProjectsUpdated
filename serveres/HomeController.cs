﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
namespace connectionToserver.Controllers
{
    public class HomeController : Controller
    {
        string connectionString = "Server=MYSQL5017.Smarterasp.net;Database=db_9fcd7f_ad11;Uid=9fcd7f_ad11;Pwd=;";
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public string f1()
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
            //   "Server=MYSQL5017.Smarterasp.net;Database=db_9fcd7f_ad11;Uid=9fcd7f_ad11;Pwd=YOUR_DB_PASSWORD;"
        }
        public string createTable()
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("create table t1 (age int)", myConnection);
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }
        public string createTable1()
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("create table t2 ( age int,name varchar(20) )", myConnection);
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }

        public string add_age1()
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("insert into t1(age) values( 20 ) ", myConnection);
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }
        public string add_age2(int age)
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("insert into t1(age) values("+ age+ ") ", myConnection);
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }
        public string add_age3(int age)
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("insert into t1(age) values(@age) ", myConnection);
                comand1.Parameters.Add("@age", MySqlDbType.Int16).Value = age;
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }

        public string add_to_table2(int age, string name)
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("insert into t2(age,name) values( @age,@name ) ", myConnection);
                comand1.Parameters.Add("@age", MySqlDbType.Int16).Value = age;
                comand1.Parameters.Add("@name", MySqlDbType.VarChar).Value = name;
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }

        public string getNmesFromT2()
        {
            try
            {

                MySqlDataReader reader;
                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("select * from t2 ", myConnection);

                reader = comand1.ExecuteReader();
                string name = "";
                while (reader.Read())
                {
                    name += reader[1].ToString() + @"<br\>";
                }
                return name;
            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }
        public string getAgesFromT1()
        {
            try
            {

                MySqlDataReader reader;
                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("select * from t1 ", myConnection);

                reader = comand1.ExecuteReader();
                int allAges = 0;
                while (reader.Read())
                {
                    allAges += int.Parse(reader[0].ToString());
                }
                return allAges+"";
            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }
        public string addToPersons(string name, string id, int age, string adress)
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("insert into persons1 (name,idd,age,adress) values(@name,@idd,@age,@adress)", myConnection);
                comand1.Parameters.Add("@name",MySqlDbType.VarChar).Value = name;
                comand1.Parameters.Add("@adress", MySqlDbType.VarChar).Value = adress;
                comand1.Parameters.Add("@idd", MySqlDbType.VarChar).Value = id;
                comand1.Parameters.Add("@age", MySqlDbType.Int16).Value = age;
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }


        public string createTablePersons()
        {
            try
            {


                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("create table persons1 ( age int,name varchar(20),idd varchar(20),adress varchar(20) )", myConnection);
                comand1.ExecuteNonQuery();


                return "ok";

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }

        public string getAllPersons()
        {
            try
            {

                MySqlDataReader reader;
                MySqlConnection myConnection = new MySqlConnection(connectionString);
                myConnection.Open();

                MySqlCommand comand1 = new MySqlCommand("select * from persons1 ", myConnection);
                reader = comand1.ExecuteReader();
                string name, adress, age, idd,all="";
                while (reader.Read())
                {
                    age = reader[0].ToString();
                    name = reader[1].ToString();
                    idd = reader[2].ToString();
                    adress = reader[3].ToString();
                    all += age + "  " + name + "  " + idd + "  " + adress + "  \n";
                }


                return all;

            }
            catch (MySqlException e1)
            {
                return e1.ToString();
            }
        }

    }
}
