package net.crofis.backendlesstododemo.Data;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.geo.GeoPoint;
import com.backendless.persistence.BackendlessDataQuery;

public class assignments
{
  private java.util.Date created;
  private java.util.Date updated;
  private String objectId;
  private String title;
  private String ownerId;
  public java.util.Date getCreated()
  {
    return created;
  }

  public java.util.Date getUpdated()
  {
    return updated;
  }

  public String getObjectId()
  {
    return objectId;
  }

  public String getTitle()
  {
    return title;
  }

  public void setTitle( String title )
  {
    this.title = title;
  }

  public String getOwnerId()
  {
    return ownerId;
  }

                                                    
  public assignments save()
  {
    return Backendless.Data.of( assignments.class ).save( this );
  }

  public Future<assignments> saveAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<assignments> future = new Future<assignments>();
      Backendless.Data.of( assignments.class ).save( this, future );

      return future;
    }
  }

  public void saveAsync( AsyncCallback<assignments> callback )
  {
    Backendless.Data.of( assignments.class ).save( this, callback );
  }

  public Long remove()
  {
    return Backendless.Data.of( assignments.class ).remove( this );
  }

  public Future<Long> removeAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<Long> future = new Future<Long>();
      Backendless.Data.of( assignments.class ).remove( this, future );

      return future;
    }
  }

  public void removeAsync( AsyncCallback<Long> callback )
  {
    Backendless.Data.of( assignments.class ).remove( this, callback );
  }

  public static assignments findById( String id )
  {
    return Backendless.Data.of( assignments.class ).findById( id );
  }

  public static Future<assignments> findByIdAsync( String id )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<assignments> future = new Future<assignments>();
      Backendless.Data.of( assignments.class ).findById( id, future );

      return future;
    }
  }

  public static void findByIdAsync( String id, AsyncCallback<assignments> callback )
  {
    Backendless.Data.of( assignments.class ).findById( id, callback );
  }

  public static assignments findFirst()
  {
    return Backendless.Data.of( assignments.class ).findFirst();
  }

  public static Future<assignments> findFirstAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<assignments> future = new Future<assignments>();
      Backendless.Data.of( assignments.class ).findFirst( future );

      return future;
    }
  }

  public static void findFirstAsync( AsyncCallback<assignments> callback )
  {
    Backendless.Data.of( assignments.class ).findFirst( callback );
  }

  public static assignments findLast()
  {
    return Backendless.Data.of( assignments.class ).findLast();
  }

  public static Future<assignments> findLastAsync()
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<assignments> future = new Future<assignments>();
      Backendless.Data.of( assignments.class ).findLast( future );

      return future;
    }
  }

  public static void findLastAsync( AsyncCallback<assignments> callback )
  {
    Backendless.Data.of( assignments.class ).findLast( callback );
  }

  public static BackendlessCollection<assignments> find( BackendlessDataQuery query )
  {
    return Backendless.Data.of( assignments.class ).find( query );
  }

  public static Future<BackendlessCollection<assignments>> findAsync( BackendlessDataQuery query )
  {
    if( Backendless.isAndroid() )
    {
      throw new UnsupportedOperationException( "Using this method is restricted in Android" );
    }
    else
    {
      Future<BackendlessCollection<assignments>> future = new Future<BackendlessCollection<assignments>>();
      Backendless.Data.of( assignments.class ).find( query, future );

      return future;
    }
  }

  public static void findAsync( BackendlessDataQuery query, AsyncCallback<BackendlessCollection<assignments>> callback )
  {
    Backendless.Data.of( assignments.class ).find( query, callback );
  }
}