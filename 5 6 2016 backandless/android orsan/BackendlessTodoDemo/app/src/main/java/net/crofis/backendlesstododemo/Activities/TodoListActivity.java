package net.crofis.backendlesstododemo.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.github.clans.fab.FloatingActionButton;

import net.crofis.backendlesstododemo.Adapters.TodoItemAdapter;
import net.crofis.backendlesstododemo.Data.assignments;
import net.crofis.backendlesstododemo.R;

import java.util.ArrayList;

public class TodoListActivity extends AppCompatActivity {

    private static final String TAG = "TodoListActivity";

    RecyclerView items;
    TodoItemAdapter adapter;

    FloatingActionButton fabAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);

        initUI();
    }

    /** Find all views*/
    private void initUI(){
        items = (RecyclerView) findViewById(R.id.todo_list_activity_items);

        fabAdd = (FloatingActionButton)findViewById(R.id.fab_add_item);

        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentDialog();
            }
        });

        //Fetch all items
        getData();
    }

    /** Present the dialog */
    private void presentDialog() {
        /** Build the dialog with "Add item" title and inflate the EditText view*/
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        /** Get the inflater */
        LayoutInflater inflater = this.getLayoutInflater();
        /** Inflate the view */
        final View dialogView = inflater.inflate(R.layout.dialog_item_add, null);
        /** Set view to dialog */
        dialogBuilder.setView(dialogView);
        /** Set Title */
        dialogBuilder.setTitle("Add item");
        /** Set positive button */
        dialogBuilder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                /** Find the EditText to get item data to add */
                EditText editText = (EditText) dialogView.findViewById(R.id.dialog_input_text);
                /** Check if it is empty or not */
                if(!editText.getText().toString().trim().isEmpty())
                {
                    /** Create new object */
                    assignments todo = new assignments();
                    /** Set properties */
                    todo.setTitle(editText.getText().toString().trim());
                    /** Because this class was generated it will have a save function */
                    todo.saveAsync(new AsyncCallback<assignments>() {
                        @Override
                        public void handleResponse(assignments response) {
                            Toast.makeText(TodoListActivity.this, "Success", Toast.LENGTH_SHORT).show();
                            /** Refresh data */
                            getData();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                            Toast.makeText(TodoListActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else {
                    Toast.makeText(TodoListActivity.this, "You cannot add an empty item.", Toast.LENGTH_SHORT).show();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        /** Show alert dialog */
        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    /** Fetch all the items */
    private void getData() {
        /** Add WhereClause statement to fetch all the data that is owned by the current user using ownerId property */
        String whereClause = "ownerId = '" + Backendless.UserService.CurrentUser().getObjectId() + "'";
        /** BackendLessDataQuery is used to filter data and configure the query */
        BackendlessDataQuery data = new BackendlessDataQuery();
        /** By default backendLess only return 10 items on Android sdk, so we set the page size to get more than that */
        data.setPageSize(100);
        /** Set the whereClause */
        data.setWhereClause(whereClause);
        /** Queries the backendLess Database and return all the data for the specific user, here it returns all the todo list */
        Backendless.Persistence.of(assignments.class).find(data, new AsyncCallback<BackendlessCollection<assignments>>() {
            @Override
            public void handleResponse(final BackendlessCollection<assignments> response) {
                /** Create adapter, add layoutManager to recyclerView then display the items, also add swipe to delete item*/
                adapter = new TodoItemAdapter(TodoListActivity.this, (ArrayList<assignments>) response.getData());
                items.addItemDecoration(new DividerItemDecoration(TodoListActivity.this,LinearLayoutManager.VERTICAL));
                items.setAdapter(adapter);
                items.setLayoutManager(new LinearLayoutManager(TodoListActivity.this));
                ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                        /** remove the exact item in database, just pass the item you get from the arraylist to this function */
                        Backendless.Persistence.of(assignments.class).remove(response.getData().get(viewHolder.getAdapterPosition()), new AsyncCallback<Long>() {
                            @Override
                            public void handleResponse(Long response) {
                                Toast.makeText(TodoListActivity.this, "Removed", Toast.LENGTH_SHORT).show();
                                getData();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                Log.e(TAG, "handleFault: " + fault.getMessage());
                            }
                        });
                    }
                };

                ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
                itemTouchHelper.attachToRecyclerView(items);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
                Toast.makeText(TodoListActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /** Inflate menu for logout function */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.todo_list_menu,menu);
        return true;
    }

    /** Handle menu click listener */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.todo_list_logout)
        {
            Backendless.UserService.logout(new AsyncCallback<Void>() {
                @Override
                public void handleResponse(Void response) {
                    Intent intent = new Intent(TodoListActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "handleFault: " + fault.getMessage());
                    Toast.makeText(TodoListActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        return true;
    }

    /** Just to add lines to recyclerview Credits: androidhive.com */
    class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{
                android.R.attr.listDivider
        };

        public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

        public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

        private Drawable mDivider;

        private int mOrientation;

        public DividerItemDecoration(Context context, int orientation) {
            final TypedArray a = context.obtainStyledAttributes(ATTRS);
            mDivider = a.getDrawable(0);
            a.recycle();
            setOrientation(orientation);
        }

        public void setOrientation(int orientation) {
            if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
                throw new IllegalArgumentException("invalid orientation");
            }
            mOrientation = orientation;
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            if (mOrientation == VERTICAL_LIST) {
                drawVertical(c, parent);
            } else {
                drawHorizontal(c, parent);
            }
        }

        public void drawVertical(Canvas c, RecyclerView parent) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin;
                final int bottom = top + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        public void drawHorizontal(Canvas c, RecyclerView parent) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int left = child.getRight() + params.rightMargin;
                final int right = left + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            if (mOrientation == VERTICAL_LIST) {
                outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
            } else {
                outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
            }
        }
    }

}