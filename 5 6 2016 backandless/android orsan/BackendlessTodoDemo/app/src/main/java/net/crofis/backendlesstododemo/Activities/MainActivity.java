package net.crofis.backendlesstododemo.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.local.UserIdStorageFactory;

import net.crofis.backendlesstododemo.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    LinearLayout holder;
    TextView email,password;
    Button login,register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * In a nutshell
         * 1. Check for user session to see if a session exists, if so change activity to the list activity
         *  a. This is done by using Backendless.Userservice.isValidLogin(callback) which returns boolean;
         *  b. If true means that user exists, so we get its objectId using UserIdStorageFactory.instance().getStorage().get();
         *  c. Then we use this id to fetch the data back from backendless database and save it inside Backendless.UserService.setCurrentUser(user);
         * 2. If no session found display the login register bottoms with the edittexts
         * 3. If user clicked register
         *  a. It takes the email and password and run Backendless.UserService.register(email,password,callback) function, thats it, and returns the BackendlessUser object
         * 4. If user clicked login
         *  a. It takes the email and password and run Backendless.UserService.login(email,password,callback) function, and returns BackendlessUser object
         *  b. It then changes activity
         * */

        initUI();

    }

    private void initUI(){
        //CheckForSessionFirst

        /** Note: holder is the container for the EditTexts and Buttons, and its invisible */
        holder = (LinearLayout)findViewById(R.id.activity_main_login_layout);

        email = (TextView)findViewById(R.id.activity_main_login_layout_email_text);
        password = (TextView)findViewById(R.id.activity_main_login_layout_password_text);

        login = (Button)findViewById(R.id.activity_main_login_layout_login_button);
        register = (Button)findViewById(R.id.activity_main_login_layout_register_button);


        /** Handle the login button */
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Check if email contains @ */
                if(checkEmailSyntax())
                {
                    /** Run Backendless login */
                    Backendless.UserService.login(email.getText().toString().trim(), password.getText().toString(), new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            /** Success, the user is logged in */
                            transitActivity();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                            Toast.makeText(MainActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    },true);
                }
                else
                {
                    Toast.makeText(MainActivity.this, "Wrong email input", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /** Handle register button */
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Check if email contains @ */
                if(checkEmailSyntax())
                {
                    /** Create new BackendlessUser object */
                    BackendlessUser user = new BackendlessUser();
                    /** set email and password properties */
                    user.setEmail(email.getText().toString().trim());
                    user.setPassword(password.getText().toString());
                    /** Save the object AKA register */
                    Backendless.UserService.register(user, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            Toast.makeText(MainActivity.this, "Success, press login", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                            Toast.makeText(MainActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        /** Check for session */
        checkForSession();
    }

    /** Check for user session */
    private void checkForSession() {
        /** See if session is valid */
        Backendless.UserService.isValidLogin(new AsyncCallback<Boolean>() {
            @Override
            public void handleResponse(Boolean response) {
                if(response)
                {
                    /** If true get the objectId of the user and fetch it from backendless */
                    String userId = UserIdStorageFactory.instance().getStorage().get();
                    Backendless.Persistence.of(BackendlessUser.class).findById(userId, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser response) {
                            Backendless.UserService.setCurrentUser(response);
                            transitActivity();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.e(TAG, "handleFault: " + fault.getMessage());
                            Toast.makeText(MainActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else
                {
                    /** If it doesn't make the container visible */
                    holder.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.e(TAG, "handleFault: " + fault.getMessage());
                Toast.makeText(MainActivity.this, fault.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /** This just checks the email field if it contains @ */
    private boolean checkEmailSyntax() {
        if(email.getText().toString().contains("@"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /** Move to the after login activity */
    private void transitActivity(){
        Intent intent = new Intent(this,TodoListActivity.class);
        startActivity(intent);
        finish();
    }
}
