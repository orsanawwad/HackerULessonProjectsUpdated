package net.crofis.backendlesstododemo.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.crofis.backendlesstododemo.Data.assignments;
import net.crofis.backendlesstododemo.R;

import java.util.ArrayList;

/**
 * Created by orsanawwad on 04/06/2016.
 */
public class TodoItemAdapter extends RecyclerView.Adapter<TodoItemAdapter.ViewHolder> {

    Context context;
    ArrayList<assignments> data;
    LayoutInflater inflater;

    public TodoItemAdapter(Context context, ArrayList<assignments> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_todo,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        assignments current = data.get(position);
        holder.title.setText(current.getTitle());
        //TODO: Format the date
        holder.date.setText(current.getCreated().toString());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder
    {

        TextView title,date;
        public ViewHolder(View v) {
            super(v);
            title = (TextView)v.findViewById(R.id.todo_item_title);
            date = (TextView)v.findViewById(R.id.todo_item_date);
        }
    }
}
